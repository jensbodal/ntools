﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ntools.ACEvents.StructEnum
{
    class StanceMode
    {
        /// <summary>
        /// melee UA weapon with no shield in attack stance	
        /// </summary>
        public const int MeleeUANoShield = 0x3C;
        /// <summary>
        /// Standing	
        /// </summary>
        public const int Standing = 0x3D;
        /// <summary>
        /// melee weapon with no shield in attack stance	
        /// </summary>
        public const int MeleeNoShield = 0x3E;
        /// <summary>
        /// bow in attack stance	
        /// </summary>
        public const int Bow = 0x3F;
        /// <summary>
        /// melee weapon with shield in attack stance	
        /// </summary>
        public const int MeleeWithShield = 0x40;
        /// <summary>
        /// Spellcasting	
        /// </summary>
        public const int Spellcasting = 0x49;
    }
}
