﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ntools.ACEvents.StructEnum
{
    public static class GameActions
    {
        /// <summary>Set a single character option.</summary>
        public const int SetSingleCharacterOption = 0x0005;

        /// <summary>Set AFK message.</summary>
        public const int SetAfkMessage = 0x0010;

        /// <summary>Store an item in a container.</summary>
        public const int StoreItem = 0x0019;

        /// <summary>Equip an item.</summary>
        public const int EquipItem = 0x001A;

        /// <summary>Drop an item.</summary>
        public const int DropItem = 0x001B;

        /// <summary>Attempt to use an item.</summary>
        public const int UseItem = 0x0036;

        /// <summary>Spend XP to raise a vital.</summary>
        public const int RaiseVital = 0x0044;

        /// <summary>Spend XP to raise an attribute.</summary>
        public const int RaiseAttribute = 0x0045;

        /// <summary>Spend XP to raise a skill.</summary>
        public const int RaiseSkill = 0x0046;

        /// <summary>Spend skill credits to train a skill.</summary>
        public const int TrainSkill = 0x0047;

        /// <summary>Cast a spell.</summary>
        public const int CastSpell = 0x0048;

        /// <summary>Cast a spell.</summary>
        public const int CastSpellOnObject = 0x004A;

        /// <summary>The client is ready for the character to materialize after portalling or logging on.</summary>
        public const int Materialize = 0x00A1;

        /// <summary>Give an item to someone.</summary>
        public const int GiveItem = 0x00CD;

        /// <summary>Add an item to the shortcut bar.</summary>
        public const int MakeShortcut = 0x019C;

        /// <summary>Remove an item from the shortcut bar.</summary>
        public const int RemoveShortcut = 0x019D;

        /// <summary>Set multiple character options.</summary>
        public const int SetCharacterOptions = 0x01A1;

        /// <summary>Add a spell to a spell bar.</summary>
        public const int AddSpellToSpellbar = 0x01E3;

        /// <summary>Remove a spell from a spell bar.</summary>
        public const int RemoveSpellFromSpellbar = 0x01E4;
    }
}
