﻿using System;

namespace Ntools.ACEvents.StructEnum
{
    public static class DisplayStatusMessage
    {
        // You're too busy!
        public const int TooBusy = 0x001D;
        // You are too fatigued to attack!
        public const int TooFatigued = 0x03F7;
        // You are out of ammunition!
        public const int OutOfAmmo = 0x03F8;
        // Your missile attack misfired!
        public const int MissileMisfire = 0x03F9;
        // You've attempted an impossible spell path!
        public const int ImpossiblePath = 0x03FA;
        // You don't know that spell!
        public const int UnknownSpell = 0X03FE;
        // Incorrect target type
        public const int IncorrectTargetType = 0X03FF;
        // You don't have all the components for this spell.
        public const int MissingComponents = 0x0400;
        // You don't have enough Mana to cast this spell.
        public const int NotEnoughMana = 0x0401;
        // Your spell fizzled.
        public const int SpellFizzle = 0x0402;
        // Your spell's target is missing!
        public const int SpellTargetMissing = 0x0403;
        // Your projectile spell mislaunched!
        public const int ProjectileMislaunch = 0x0404;
        // You have solved this quest too recently!
        public const int SolvedQuestTooRecently = 0x043E;
        // You have solved this quest too many times!
        public const int SolvedQuestTooMany = 0x043F;
        // You have entered your allegiance chat room.
        public const int EnteredAllegianceChat = 0x051B;
        // You have left an allegiance chat room.
        public const int LeftAllegianceRoom = 0x051C;
        // Turbine Chat is enabled.
        public const int TurbineChatEnabled = 0x051D;

        public static string GetDisplayStatusMessage(int statusMessage) {
            switch (statusMessage) {
                case TooBusy:
                    return "You're too busy";
                default:
                    return "Not yet implemented";
            }
        }
    }
}
