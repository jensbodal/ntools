﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ntools.ACEvents.StructEnum
{
    /// <summary>
    /// Contains all Sub-Messages of the GameEvent event
    /// <example>Accessed via: <code>args.Message.Value&lt;uint>("event")</code></example>
    /// </summary>
    public static class GameEvents {
        /// <summary>Display a message in a popup message window.</summary>
        public const int MessageBox = 0x0004;

        /// <summary>Information describing your character.</summary>
        public const int LoginCharacter = 0x0013;

        /// <summary>Returns info related to your monarch, patron and vassals.</summary>
        public const int AllegianceInfo = 0x0020;

        /// <summary>Store an item in a container.</summary>
        public const int InsertInventoryItem = 0x0022;

        /// <summary>Equip an item.</summary>
        public const int WearItem = 0x0023;

        /// <summary>Titles for the current character.</summary>
        public const int TitleList = 0x0029;

        /// <summary>Set a title for the current character.</summary>
        public const int SetTitle = 0x002b;

        /// <summary>Close Container - Only sent when explicitly closed</summary>
        public const int CloseContainer = 0x0052;

        /// <summary>Open the buy/sell panel for a merchant.</summary>
        public const int ApproachVendor = 0x0062;

        /// <summary>Failure to give an item</summary>
        public const int FailureToGiveItem = 0x00A0;

        /// <summary>Member left fellowship</summary>
        public const int FellowshipMemberQuit = 0x00A3;

        /// <summary>Member dismissed from fellowship</summary>
        public const int FellowshipMemberDismissed = 0x00A4;

        /// <summary>Sent when you first open a book, contains the entire table of contents.</summary>
        public const int ReadTableOfContents = 0x00B4;

        /// <summary>Contains the text of a single page of a book, parchment or sign.</summary>
        public const int ReadPage = 0x00B8;

        /// <summary>The result of an attempt to assess an item or creature.</summary>
        public const int IdObject = 0x00C9;

        /// <summary>Group Chat</summary>
        public const int GroupChat = 0x0147;

        /// <summary>Set Pack Contents</summary>
        public const int SetPackContents = 0x0196;

        /// <summary>Removes an item from inventory (when you place it on the ground or give it away)</summary>
        public const int DropFromInventory = 0x019A;

        /// <summary>Melee attack completed</summary>
        public const int AttackCompleted = 0x01A7;

        /// <summary>Delete a spell from your spellbook.</summary>
        public const int DeleteSpellFromSpellbook = 0x01A8;

        /// <summary>You just died.</summary>
        public const int YourDeath = 0x01AC;

        /// <summary>Message for a death, something you killed or your own death message.</summary>
        public const int KillOrDeathMessage = 0x01AD;

        /// <summary>You have hit your target with a melee attack.</summary>
        public const int InflictMeleeDamage = 0x01B1;

        /// <summary>You have been hit by a creature's melee attack.</summary>
        public const int ReceiveMeleeDamage = 0x01B2;

        /// <summary>Your target has evaded your melee attack.</summary>
        public const int OtherMeleeEvade = 0x01B3;

        /// <summary>You have evaded a creature's melee attack.</summary>
        public const int SelfMeleeEvade = 0x01B4;

        /// <summary>Start melee attack</summary>
        public const int StartMeleeAttack = 0x01B8;

        /// <summary>Update a creature's health bar.</summary>
        public const int UpdateHealth = 0x01C0;

        /// <summary>Age Command Result - happens when you do /age in the game</summary>
        public const int AgeCommandResult = 0x01C3;

        /// <summary>Ready.  Previous action complete</summary>
        public const int ReadyPreviousActionComplete = 0x01C7;

        /// <summary>Update Allegiance info, sent when allegiance panel is open</summary>
        public const int UpdateAllegianceInfo = 0x01C8;

        /// <summary>Close Assess Panel</summary>
        public const int CloseAssessPanel = 0x01CB;

        /// <summary>Ping Reply</summary>
        public const int PingReply = 0x01EA;

        /// <summary>Squelched Users List</summary>
        public const int SquelchedUsersList = 0x01F4;

        /// <summary>Send to begin a trade and display the trade window</summary>
        public const int EnterTrade = 0x01FD;

        /// <summary>End trading</summary>
        public const int EndTrade = 0x01FF;

        /// <summary>Item was added to trade window - you will receive a Create Object (0xF745) with details of the item</summary>
        public const int AddTradeItem = 0x0200;

        /// <summary>The trade was accepted</summary>
        public const int AcceptTrade = 0x0202;

        /// <summary>The trade was un-accepted</summary>
        public const int UnacceptTrade = 0x0203;

        /// <summary>The trade window was reset</summary>
        public const int ResetTrade = 0x0205;

        /// <summary>Failure to add a trade item</summary>
        public const int FailureToAddATradeItem = 0x0207;

        /// <summary>Failure to complete a trade</summary>
        public const int FailureToCompleteATrade = 0x0208;

        /// <summary>Buy a dwelling or pay maintenance</summary>
        public const int DisplayDwellingPurchaseOrMaintenancePanel = 0x021D;

        /// <summary>House panel information for owners.</summary>
        public const int HouseInformationForOwners = 0x0225;

        /// <summary>House panel information for non-owners.</summary>
        public const int HouseInformationForNonowners = 0x0226;

        /// <summary>House Guest List, Sent in response to asking for one.</summary>
        public const int HouseGuestList = 0x0257;

        /// <summary>Update an item's mana bar.</summary>
        public const int UpdateItemManaBar = 0x0264;

        /// <summary>Display a list of available dwellings in the chat window.</summary>
        public const int HousesAvailable = 0x0271;

        /// <summary>Display a confirmation panel.</summary>
        public const int ConfirmationPanel = 0x0274;

        /// <summary>A player has closed your confirmation panel.</summary>
        public const int ConfirmationPanelClosed = 0x0276;

        /// <summary>Display an allegiance login/logout message in the chat window.</summary>
        public const int AllegianceMemberLoginOrOut = 0x027A;

        /// <summary>Display a status message in the chat window.</summary>
        public const int DisplayStatusMessage = 0x028A;

        /// <summary>Display a parameterized status message in the chat window.</summary>
        public const int DisplayParameterizedStatusMessage = 0x028B;

        /// <summary>Set Turbine Chat channel numbers.</summary>
        public const int SetTurbineChatChannels = 0x0295;

        /// <summary>Someone has sent you a @tell.</summary>
        public const int Tell = 0x02BD;

        /// <summary>Create or join a fellowship</summary>
        public const int CreateFellowship = 0x02BE;

        /// <summary>Disband your fellowship.</summary>
        public const int DisbandFellowship = 0x02BF;

        /// <summary>Add a member to your fellowship.</summary>
        public const int AddFellowshipMember = 0x02C0;

        /// <summary>Add a spell to your spellbook.</summary>
        public const int AddSpellToSpellbook = 0x02C1;

        /// <summary>Apply an enchantment to your character.</summary>
        public const int AddCharacterEnchantment = 0x02C2;

        /// <summary>Remove an enchantment from your character.</summary>
        public const int RemoveCharacterEnchantment = 0x02C3;

        /// <summary>Remove multiple enchantments from your character.</summary>
        public const int RemoveMultipleCharacterEnchantments = 0x02C5;

        /// <summary>Silently remove all enchantments from your character, e.g. when you die (no message in the chat window).</summary>
        public const int RemoveAllCharacterEnchantments_Silent = 0x02C6;

        /// <summary>Silently remove An enchantment from your character.</summary>
        public const int RemoveCharacterEnchantment_Silent = 0x02C7;

        /// <summary>Silently remove multiple enchantments from your character (no message in the chat window).</summary>
        public const int RemoveMultipleCharacterEnchantments_Silent = 0x02C8;

        /// <summary>A portal storm is brewing.</summary>
        public const int MildPortalStorm = 0x02C9;

        /// <summary>A portal storm is imminent.</summary>
        public const int HeavyPortalStorm = 0x02CA;

        /// <summary>You have been portal stormed.</summary>
        public const int PortalStormed = 0x02CB;

        /// <summary>The portal storm has subsided.</summary>
        public const int EndPortalStorm = 0x02CC;

        /// <summary>Display a status message on the Action Viewscreen (the red text overlaid on the 3D area).</summary>
        public const int StatusMessage = 0x02EB;

        public static string GetMessage(uint gameEvent) {
            string eventMessage;
            switch (gameEvent) {
                case AddCharacterEnchantment:
                    eventMessage = "Add Character Enchantment";
                    break;
                case UpdateAllegianceInfo:
                    eventMessage = "Update Allegiance Info";
                    break;
                case ReadyPreviousActionComplete:
                    eventMessage = "Ready. Previous action complete";
                    break;
                case DisplayStatusMessage:
                    eventMessage = "Display a status message in the chat window";
                    break;
                case IdObject:
                    eventMessage = "The result of an attempt to assess an item or creature";
                    break;
                case UpdateHealth:
                    eventMessage = "Update a creature's health bar";
                    break;
                default:
                    eventMessage = "Not yet implemented";
                    break;
            }
            return ReturnMessage(eventMessage, gameEvent);
        }

        private static string ReturnMessage(string message, uint gameEvent) {
            return String.Format("{0:X}: {1}", gameEvent, message);
        }
    }
}
