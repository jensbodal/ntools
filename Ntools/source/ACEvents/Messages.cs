﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ntools.ACEvents
{
    /// <summary>
    /// Contains all Messages that I am processing; detailed here: 
    /// http://skunkworks.sourceforge.net/protocol/Protocol.php
    /// </summary>
    public static class MessageTypes {

        /// <summary>These are animations. Whenever a human, wObject or object moves - one of 
        /// these little messages is sent. Even idle emotes (like head scratching and nodding) are
        /// sent in this manner.</summary>
        public const int Animation = 0xF74C;

        /// <summary>Contains the text associated with an emote action.</summary>
        public const int EmoteText = 0x01E2;

        /// <summary>Game Actions are outgoing messages that are sequenced.</summary>
        public const int GameAction = 0xF7B1;

        /// <summary> See GameEvents for all events related to GameEvent
        /// GameEvents are structured </summary>
        public const int GameEvent = 0xF7B0;

        /// <summary>Indirect '/e' text.</summary>
        public const int IndirectText = 0x01E0;

        /// <summary>
        /// Set position - the server pathologically sends these after every actions - 
        /// sometimes more than once. If has options for setting a fixed velocity or an arc for
        /// thrown weapons and arrows.
        /// </summary>
        /// <remarks>
        /// <para>object ("ObjectID") = The object with the position changing</para>
        /// <para>position ("Position") = The current or starting location</para>
        /// <para>logins ("WORD") = logins</para>
        /// <para>sequence ("WORD") = A sequence number of some sort</para>
        /// <para>portals ("WORD") = number of portals</para>
        /// <para>adjustments("WORD") = Adjustments to position</para>
        /// </remarks>
        public const int SetPositionAndMotion = 0xF748;

        public static string GetMessageText(int message) {
            switch (message) {
                case Animation:
                    return "Animation has triggered";
                case EmoteText:
                    return "Contains the text associated with an emote action.";
                case GameAction:
                    return "Game Actions are outgoing messages that are sequenced.";
                case GameEvent:
                    return "GameEvent";
                case IndirectText:
                    return "Indirect '/e' text";
                case SetPositionAndMotion:
                    return "Set position - the server pathologically sends these after every " +
                    "actions - sometimes more than once. If has options for setting a fixed " + 
                    "velocity or an arc for thrown weapons and arrows.";
                default:
                    return "Message text not yet implemented";
            }
        }
    }
}
