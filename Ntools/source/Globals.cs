﻿using System;

using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Ntools.Engine;

namespace Ntools {
    public static class Globals {
        public static void Init(string pluginName, PluginHost host, CoreManager core) {
            PluginName = pluginName;
            Host = host;
            Core = core;
        }

        public static string PluginName { get; private set; }
        public static PluginHost Host { get; private set; }
        public static CoreManager Core { get; private set; }
        public static SettingsEngine Settings { get; set; }
        public static CombatEngine CombatEngine { get; set; }
        public static NavigationEngine NavigationEngine { get; set; }
        public static DetectionEngine DetectionEngine { get; set; }
        public static ChatEngine ChatEngine { get; set; }

    }
}
