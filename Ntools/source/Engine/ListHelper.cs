﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using Ntools.Utilities;
using Decal.Adapter.Wrappers;

namespace Ntools.Engine {
    public static class ListHelper {
        public const int NameCol = 0;
        public const int CoordsCol = 1;
        public const int IdCol = 2;

        public static void AddToGlobalListView(WorldObject wo) {
            try {
                bool contains = false;
                bool includeInventory = Globals.Settings.chkGlobalSearchIncludeInventory.Checked;
                bool isInInventory = IsInInventory(wo);
                // If is in inventory and search inventory checked OR is not in inventory then add
                if (isInInventory && includeInventory || !isInInventory) {
                    for (int i = 0; i < Globals.Settings.lstGlobalItemView.RowCount; i++) {
                        string worldObjectName = (string)Globals.Settings.lstGlobalItemView[i][NameCol][0];
                        int worldObjectId = (int)Globals.Settings.lstGlobalItemView[i][IdCol][0];
                        if (worldObjectId == wo.Id) {
                            contains = true;
                        }
                    }
                    if (!contains) {
                        Util.WriteToChat(string.Format("Detected: {0} ({1})", wo.Name, wo.Coordinates()));
                        ListRow row = Globals.Settings.lstGlobalItemView.Add();
                        row[NameCol][0] = wo.Name;
                        row[CoordsCol][0] = wo.Coordinates().ToString();
                        row[IdCol][0] = wo.Id;
                    }
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public static WorldObject GetWorldObject(ListRow row) {
            return Globals.Core.WorldFilter[(int)row[IdCol][0]];
        }

        public static bool IsInInventory(WorldObject itemToCheck) {
            int worldObjectContainer = itemToCheck.Container;
            bool isInCharacterMainPack = Globals.Core.CharacterFilter.Id == worldObjectContainer;
            bool isInCharacterOtherPacks = false;
            WorldObjectCollection inventory = Globals.Core.WorldFilter.GetInventory();
            foreach (WorldObject wo in inventory) {
                if (wo.ObjectClass == ObjectClass.Container && worldObjectContainer == wo.Id) {
                    isInCharacterOtherPacks = true;
                }
            }
            
            return (isInCharacterMainPack || isInCharacterOtherPacks);
        }

        public static void RemoveFromGlobalListView(WorldObject wo) {
            try {
                for (int i = 0; i < Globals.Settings.lstGlobalItemView.RowCount; i++) {
                    int worldObjectId = (int)Globals.Settings.lstGlobalItemView[i][IdCol][0];
                    if (worldObjectId == wo.Id) {
                        Globals.Settings.lstGlobalItemView.Delete(i);
                    }
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public static void AddToGlobalItemSearchList(string worldObjectName) {
            try {
                if (!IsInGlobalItemSearchList(worldObjectName)) {
                    ListRow row = Globals.Settings.lstGlobalItems.Add();
                    row[NameCol][0] = worldObjectName;
                    SaveGlobalItemSearchList(typeof(string), "GlobalItemSearchList.xml");
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public static void RemoveFromGlobalItemSearchList(string worldObjectName) {
            try {
                for (int i = 0; i < Globals.Settings.lstGlobalItemView.RowCount; i++) {
                    string itemName = (string)Globals.Settings.lstGlobalItems[i][NameCol][0];
                    if (itemName.Equals(worldObjectName)) {
                        Globals.Settings.lstGlobalItemView.Delete(i);
                    }
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public static void RemoveFromGlobalItemSearchList(int row) {
            try {
                Globals.Settings.lstGlobalItems.Delete(row);
                SaveGlobalItemSearchList(typeof(string), "GlobalItemSearchList.xml");
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public static bool IsInGlobalItemSearchList(string worldObjectName) {
            bool contains = false;
            for (int i = 0; i < Globals.Settings.lstGlobalItems.RowCount; i++) {
                string globalListItemName = (string)Globals.Settings.lstGlobalItems[i][NameCol][0];
                globalListItemName = globalListItemName.ToLower();
                worldObjectName = worldObjectName.ToLower();
                
                if (worldObjectName.Contains(globalListItemName)) {
                    contains = true;
                    return contains;
                }
            }
            return contains;
        }

        public static void SaveGlobalItemSearchList(Type type, string outputFile) {
            try {
                List<string> aList = new List<string>();
                for (int row = 0; row < Globals.Settings.lstGlobalItems.RowCount; row++) {
                    string itemName = (string)Globals.Settings.lstGlobalItems[row][NameCol][0];
                    aList.Add(itemName);
                }
                SaveAndSerializeList(aList, SettingsEngine.OutputFolder, outputFile);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public static void SaveAndSerializeList<T>(List<T> list, string folderPath, string fileName) {
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
            Directory.CreateDirectory(folderPath);
            string filePath = folderPath + fileName;
            System.IO.StreamWriter file = new System.IO.StreamWriter(filePath);
            serializer.Serialize(file, list);
            file.Close();
        }

        public static List<T> LoadAndDeserializeList<T>(string filePath) {
            List<T> aList = new List<T>();
            XmlSerializer deserializer = new XmlSerializer(typeof(List<T>));
            StreamReader fileReader = new StreamReader(filePath);
            aList = (List<T>)deserializer.Deserialize(fileReader);
            fileReader.Close();
            return aList;
        }

        public static void LoadGlobalItemSearchList() {
            try {
                List<string> aList = new List<string>();
                XmlSerializer deserializer = new XmlSerializer(typeof(List<string>));
                string inputFile = SettingsEngine.OutputFolder + "GlobalItemSearchList.xml";
                if (!File.Exists(inputFile)) {
                    SettingsEngine.CreateEmptyXmlFile(inputFile, "ArrayOfString");
                }
                StreamReader file = new StreamReader(inputFile);
                aList = (List<string>)deserializer.Deserialize(file);
                file.Close();
                foreach (string name in aList) {
                    ListRow row = Globals.Settings.lstGlobalItems.Add();
                    row[NameCol][0] = name;
                }
            }
            catch (FileNotFoundException fnfe) {
                Util.LogError(fnfe);
            }
            catch (Exception ex) { Util.LogError(ex); }

        }

    }
}

