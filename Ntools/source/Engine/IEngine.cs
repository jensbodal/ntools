﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ntools.ACObjects;
using Decal.Adapter.Wrappers;

namespace Ntools.Engine {
    public interface IEngine {
        void RegisterEvents();
        void UnregisterEvents();
        void CharacterFilter_Logoff(object sender, LogoffEventArgs e);
    }
}
