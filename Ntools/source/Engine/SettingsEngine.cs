﻿using System;

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using Decal.Adapter;
using Decal.Adapter.Wrappers;

using Ntools.DecalObjects;
using Ntools.Utilities;

namespace Ntools.Engine {
    
    public class SettingsEngine {
        // Main
        public CheckBoxWrapper chkEnableDetection { get; set; }
        public CheckBoxWrapper chkStartCombat { get; set; }
        private PushButtonWrapper _btnSaveSettings;
        public PushButtonWrapper btnSaveSettings {
            get { return _btnSaveSettings; }
            set {
                _btnSaveSettings = value;
                btnSaveSettings.Click += btnSaveSettings_Click;                   
            }
        }
        public PushButtonWrapper btnSaveWindowPosition { get; set; }
        private CheckBoxWrapper _chkShowPluginOnLogin;
        public CheckBoxWrapper chkShowPluginOnLogin {
            get { return _chkShowPluginOnLogin; }
            set {
                _chkShowPluginOnLogin = value;
                chkShowPluginOnLogin.Change += chkShowPluginOnLogin_Change;
            }
        }

        // *** Combat Engine *** //
        public PushButtonWrapper btnTestDistance { get; set; }

        // *** Navigati8on Engine *** //
        // Coords Tracker
        public PushButtonWrapper btnFaceCoords { get; set; }
        public TextBoxWrapper txtFaceCoords { get; set; }
        public ListWrapper lstCoords { get; set; }
        public PushButtonWrapper btnGetCoords { get; set; }
        public PushButtonWrapper btnGotoCoords { get; set; }
        public PushButtonWrapper btnGotoSelected { get; set; }

        // Route Editor
        public TextBoxWrapper txtRouteName { get; set; }
        public PushButtonWrapper btnSaveRoute { get; set; }
        public PushButtonWrapper btnRouteStartPause { get; set; }
        public PushButtonWrapper btnRouteStop { get; set; }
        public PushButtonWrapper btnRouteReset { get; set; }
        public ChoiceWrapper choiceRoute { get; set; }
        public PushButtonWrapper btnLoadRoute { get; set; }
        public PushButtonWrapper btnAddLocation { get; set; }
        public TextBoxWrapper txtWait { get; set; }
        public ListWrapper lstRoute { get; set; }

        // *** Detection Engine *** //
        // Global Search
        public TextBoxWrapper txtSearchGlobalItem { get; set; }
        public PushButtonWrapper btnSearchGlobalItem { get; set; }
        public ListWrapper lstGlobalItemView { get; set; }
        public PushButtonWrapper btnClearGlobalItem { get; set; }
        public CheckBoxWrapper chkGlobalSearchFaceOnSelect { get; set; }
        public CheckBoxWrapper chkGlobalSearchIncludeInventory { get; set; }
        // Global Settings
        public CheckBoxWrapper chkDetectCorpses { get; set; }
        public CheckBoxWrapper chkDetectLifestones { get; set; }
        public CheckBoxWrapper chkDetectPortals { get; set; }
        public TextBoxWrapper txtAddGlobalItem { get; set; }
        public PushButtonWrapper btnAddGlobalItem { get; set; }
        public PushButtonWrapper btnAddGlobalItemSelected { get; set; }
        public ListWrapper lstGlobalItems { get; set; }
        public PushButtonWrapper btnRecheckGlobalSearch { get; set; }
        public PushButtonWrapper btnGetObjectClass { get; set; }
        public TextBoxWrapper txtArmorAL { get; set; }
        public PushButtonWrapper btnAddArmorFilter { get; set; }
        public CheckBoxWrapper chkLootScrolls { get; set; }

        public static string OutputFolder;
        private string profileFileName;
        private string globalSearchFileName;
        private readonly ACObjects.PlayerSelf Self;
        private readonly string playerServer;
        private readonly string playerName;
        private readonly ViewWrapper view;

        public class GlobalListType {
            public string Name { get; set; }
            public string Coordinates { get; set; }
            public int Id { get; set; }

            private GlobalListType() {
                // Nothing here
            }

            public GlobalListType(string name, string coordinates, int id) {
                this.Name = name;
                this.Coordinates = coordinates;
                this.Id = id;
            }

        }

        private SettingsEngine() {
            // Nothing
        }

        public SettingsEngine(ViewWrapper view, ACObjects.PlayerSelf self) {
            this.Self = self;
            this.view = view;
            this.playerServer = Self.ServerName;
            this.playerName = Self.Name;
            //Util.WriteToChat(view.Position.ToString());
            
            CreateAndSetOutputFolder();
            this.profileFileName = SetAndCreateSaveFile(OutputFolder, this.playerName);
        }

        public void LoadSettings() {
            try {
                XmlDocument sFile = new XmlDocument();
                sFile.Load(this.profileFileName);

                // The only items managed by settings are those which are below
                chkEnableDetection.Checked = GetBoolFromXml(sFile, "chkEnableDetection", true);
                chkShowPluginOnLogin.Checked = GetBoolFromXml(sFile, "chkShowPluginOnLogin", true);
                chkDetectCorpses.Checked = GetBoolFromXml(sFile, "chkDetectCorpses", true);
                chkDetectLifestones.Checked = GetBoolFromXml(sFile, "chkDetectLifestones", true);
                chkDetectPortals.Checked = GetBoolFromXml(sFile, "chkDetectPortals", true);

                // This is just here to show it can be done this way
                if (chkShowPluginOnLogin.Checked) {
                    view.Activate();
                    view.Title = (view.Title + " - " + Self.Name);
                }
                SaveSettings();
            }
            catch (System.IO.FileNotFoundException ex) {
                Util.WriteToChat("No settings file found...", Util.Color_Red);
                Util.LogError(ex, true);
            }

            catch (NullReferenceException ex) {
                Util.LogError(ex, false);
                Util.WriteToChat(
                    "Exception! Make sure you have set the property in PluginCore", Util.Color_Red);
            }
            catch (XmlException ex) {
                Util.WriteToChat("Your XML file is either corrupt or blank", Util.Color_Red);
                Util.LogError(ex, false);
            }
        }

        public void SaveSettings() {
            try {
                XmlSerializer xwriter;
                xwriter = new XmlSerializer(typeof(SettingsEngine));
                System.IO.StreamWriter file = new System.IO.StreamWriter(this.profileFileName);
                xwriter.Serialize(file, this);
                file.Close();
                
                Util.WriteToChat("Settings Saved", Util.Color_Pink);
            }
            catch (Exception ex) {
                Utilities.Util.LogError(ex);
            }
        }
        
        private bool GetBoolFromXml(
            XmlDocument settingsFile, string variableName, bool defaultValue) {
            try {
                string className = this.GetType().Name;
                string xmlNode = className + "/" + variableName + "/Checked";
                return Convert.ToBoolean(settingsFile.SelectSingleNode(xmlNode).InnerText);
            }
            catch (Exception ex) {
                string defaultMessage = String.Format(
                    "No setting found for {0}, setting to default value of {1}",
                    variableName,defaultValue);
                Util.WriteToChat(defaultMessage);
                return defaultValue;
            }
        }

        private void CreateAndSetOutputFolder() {
            string userProfile = Environment.GetEnvironmentVariable("USERPROFILE").ToString();
            string outputFolder = userProfile + "\\" + Globals.PluginName + "\\" +
                "Settings\\" + this.playerServer + "\\" + this.playerName + "\\";
            SettingsEngine.OutputFolder = outputFolder;
            System.IO.Directory.CreateDirectory(outputFolder);
        }

        public string SetAndCreateSaveFile(string folder, string fileName) {
            string outputFile = OutputFolder + fileName + ".xml";
            profileFileName = outputFile;
            Util.WriteToChat(profileFileName);
            if (!File.Exists(outputFile)) {
                CreateEmptyXmlFile(outputFile, this.GetType().Name);
            }
            return profileFileName;
        }

        public static void CreateEmptyXmlFile(string filename, string rootName) {
            try {
                XmlDocument blankXml = new XmlDocument();
                XmlNode header = blankXml.CreateXmlDeclaration("1.0", "UTF-8", null);
                XmlNode rootElement = blankXml.CreateElement(rootName);
                blankXml.AppendChild(header);
                blankXml.AppendChild(rootElement);
                blankXml.Save(filename);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void chkShowPluginOnLogin_Change(object sender, CheckBoxChangeEventArgs ev) {
            try {
                if (ev.Checked) {
                    view.Activate();
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void btnSaveSettings_Click(object sender, ControlEventArgs ev) {
            try {
                SaveSettings();
            }
            catch (Exception ex) {
                Utilities.Util.LogError(ex);
            }
        }
    }
}

//void btnGlobalTestSave_Click(object sender, ControlEventArgs e) {
//    try {
//        Util.WriteToChat("Button wired up");
//        List<GlobalListType> aList = new List<GlobalListType>();
//        XmlSerializer serializer = new XmlSerializer(typeof(List<GlobalListType>));
//        string outputFile = OutputFolder + "ListSettingsTest.xml";
//        System.IO.StreamWriter file = new System.IO.StreamWriter(outputFile);
//        const int nameColumn = 0;
//        const int coordsColumn = 1;
//        const int idColumn = 2;
//        for (int row = 0; row < lstGlobalItemView.RowCount; row++) {
//            GlobalListType obj = new GlobalListType(
//                (string)lstGlobalItemView[row][nameColumn][0],
//                (string)lstGlobalItemView[row][coordsColumn][0],
//                (int)lstGlobalItemView[row][idColumn][0]);
//            aList.Add(obj);
//        }
//        serializer.Serialize(file, aList);

//        file.Close();

//    }
//    catch (Exception ex) {
//        Util.LogError(ex);
//    }
//}

//void btnGlobalTestSave_2Click(object sender, ControlEventArgs e) {
//    try {
//        Util.WriteToChat("Testing Load Settings");
//        List<GlobalListType> aList = new List<GlobalListType>();
//        XmlSerializer deserializer = new XmlSerializer(typeof(List<GlobalListType>));
//        string inputFile = OutputFolder + "ListSettingsTest.xml";
//        System.IO.StreamReader file = new System.IO.StreamReader(inputFile);
//        aList = (List<GlobalListType>)deserializer.Deserialize(file);
//        file.Close();
//        const int nameColumn = 0;
//        const int coordsColumn = 1;
//        const int idColumn = 2;
//        foreach (GlobalListType obj in aList) {
//            ListRow row = lstGlobalItemView.Add();
//            row[nameColumn][0] = obj.Name;
//            row[coordsColumn][0] = obj.Coordinates;
//            row[idColumn][0] = obj.Id;
//        }
//    }
//    catch (Exception ex) {
//        Util.LogError(ex);
//    }
//}