﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Ntools.ACObjects;
using Ntools.Utilities;

namespace Ntools.Engine {
    public class DetectionEngine : IEngine {
        private PlayerSelf self { get; set; }

        public DetectionEngine(PlayerSelf self) {
            this.self = self;
            RegisterEvents();
        }

        public void RegisterEvents() {
            Globals.Core.WorldFilter.CreateObject += WorldFilter_CreateObject;
            Globals.Core.WorldFilter.ReleaseObject += WorldFilter_ReleaseObject;
            Globals.Settings.btnSearchGlobalItem.Click += btnSearchGlobalItem_Click;
            Globals.Settings.lstGlobalItemView.Selected += lstGlobalItemView_Selected;
            Globals.Settings.btnClearGlobalItem.Click += btnClearGlobalItem_Click;
            Globals.Core.CharacterFilter.Logoff += CharacterFilter_Logoff;
            Globals.Core.ContainerOpened += Core_ContainerOpened;
            Globals.Core.WorldFilter.ApproachVendor += WorldFilter_ApproachVendor;
            Globals.Settings.btnRecheckGlobalSearch.Click += btnRecheckGlobalSearch_Click;
            Globals.Settings.lstGlobalItems.Selected += lstGlobalItems_Selected;
            Globals.Settings.btnAddGlobalItem.Click += btnAddGlobalItem_Click;
            Globals.Settings.btnAddGlobalItemSelected.Click += btnAddGlobalItemSelected_Click;
            Globals.Settings.btnGetObjectClass.Click += btnGetObjectClass_Click;
        }

        private void WorldFilter_ApproachVendor(object sender, ApproachVendorEventArgs e) {
            try {
                Decal.Adapter.Wrappers.Vendor vendor = e.Vendor;
                
                foreach (WorldObject wo in e.Vendor) {
                    ProcessWorldObject(wo);
                }
                
                vendor.Dispose(); // exception always occurs
                vendor = null;
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public void UnregisterEvents() {
            Globals.Core.WorldFilter.CreateObject -= WorldFilter_CreateObject;
            Globals.Core.WorldFilter.ReleaseObject -= WorldFilter_ReleaseObject;
            Globals.Settings.btnSearchGlobalItem.Click -= btnSearchGlobalItem_Click;
            Globals.Settings.lstGlobalItemView.Selected -= lstGlobalItemView_Selected;
            Globals.Settings.btnClearGlobalItem.Click -= btnClearGlobalItem_Click;
            Globals.Core.CharacterFilter.Logoff -= CharacterFilter_Logoff;
            Globals.Core.ContainerOpened -= Core_ContainerOpened;
            Globals.Core.WorldFilter.ApproachVendor -= WorldFilter_ApproachVendor;
            Globals.Settings.btnRecheckGlobalSearch.Click -= btnRecheckGlobalSearch_Click;
            Globals.Settings.lstGlobalItems.Selected -= lstGlobalItems_Selected;
            Globals.Settings.btnAddGlobalItem.Click -= btnAddGlobalItem_Click;
            Globals.Settings.btnAddGlobalItemSelected.Click -= btnAddGlobalItemSelected_Click;
            Globals.Settings.btnGetObjectClass.Click -= btnGetObjectClass_Click;
        }

        private void Core_ContainerOpened(object sender, ContainerOpenedEventArgs e) {
            try {
                Util.WriteToChat("Container Opened");
                if (e.ItemGuid != 0) {
                    WorldObjectCollection containerItems =
                        Globals.Core.WorldFilter.GetByContainer(e.ItemGuid);
                    System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                    timer.Interval = 100;
                    int counter = 0;
                    timer.Tick += new EventHandler((object nothing, EventArgs timerEv) => {
                        if (counter > 10) {
                            timer.Stop();
                        }
                        if (containerItems.Quantity > 0) {
                            foreach (WorldObject wo in containerItems) {
                                Util.WriteToChat(wo.Name, Util.Color_Pink);
                                ProcessWorldObject(wo);
                            }
                            timer.Stop();
                        }
                        counter++;
                    });
                    timer.Start();
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public void CharacterFilter_Logoff(object sender, LogoffEventArgs e) {
            try {
                UnregisterEvents();
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }



        //Hiro Ishigame
        private void WorldFilter_CreateObject(object sender, CreateObjectEventArgs ev) {
            try {
                ProcessWorldObject(ev.New);

            }
            catch (Exception ex) {
                Utilities.Util.LogError(ex);
            }
        }

        private void ProcessWorldObject(WorldObject wo) {
            switch (wo.ObjectClass) {
                case ObjectClass.Portal:
                    if (Globals.Settings.chkDetectPortals.Checked) {
                        ListHelper.AddToGlobalListView(wo);
                    }
                    break;
                case ObjectClass.Lifestone:
                    if (Globals.Settings.chkDetectLifestones.Checked) {
                        ListHelper.AddToGlobalListView(wo);
                    }
                    break;
                case ObjectClass.Corpse:
                    if (Globals.Settings.chkDetectCorpses.Checked) {
                        if (wo.Name.Equals("Corpse of " + self.Name)) {
                            ListHelper.AddToGlobalListView(wo);
                            Util.WriteToChat(wo.Name);
                        }
                    }
                    break;
                case ObjectClass.Monster:
                    break;
                case ObjectClass.Scroll:
                    if (Globals.Settings.chkLootScrolls.Checked) {
                        int spellId = wo.Values(LongValueKey.AssociatedSpell);
                        bool isKnown = Globals.Core.CharacterFilter.IsSpellKnown(spellId);
                        if (!isKnown) {
                            ListHelper.AddToGlobalListView(wo);
                        }
                    }
                    
                    break;
                default:
                    break;
            }
            
            // Now will check the items by name portion regardless of object class
            if (ListHelper.IsInGlobalItemSearchList(wo.Name)) {
                ListHelper.AddToGlobalListView(wo);
            }
        }

        private void WorldFilter_ReleaseObject(object sender, ReleaseObjectEventArgs ev) {
            try {
                ListHelper.RemoveFromGlobalListView(ev.Released);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public void SearchWorldByName(string name) {
            try {
                WorldObjectCollection itemsByName = Globals.Core.WorldFilter.GetAll();
                name = name.ToLower();
                foreach (WorldObject wo in itemsByName) {
                    if (wo.Name.ToLower().Contains(name)) {
                        ListHelper.AddToGlobalListView(wo);
                    }
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void btnSearchGlobalItem_Click(object sender, ControlEventArgs ev) {
            try {
                SearchWorldByName(Globals.Settings.txtSearchGlobalItem.Text);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        // Clear Global Search View List
        private void btnClearGlobalItem_Click(object sender, ControlEventArgs e) {
            try {
                Globals.Settings.lstGlobalItemView.Clear();
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void lstGlobalItemView_Selected(object sender, ListSelectEventArgs ev) {
            try {
                int row = ev.Row;
                ListWrapper globalList = Globals.Settings.lstGlobalItemView;
                WorldObject selectedWo = ListHelper.GetWorldObject(globalList[row]);
                Globals.Core.Actions.SelectItem(selectedWo.Id);
                if (Globals.Settings.chkGlobalSearchFaceOnSelect.Checked) {
                    CoordsObject selectedCoords = selectedWo.Coordinates();
                    Globals.NavigationEngine.FaceCoords(selectedCoords);
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void btnAddGlobalItem_Click(object sender, ControlEventArgs ev) {
            try {
                ListHelper.AddToGlobalItemSearchList(Globals.Settings.txtAddGlobalItem.Text);
            }
            catch (Exception ex) {
                Utilities.Util.LogError(ex);
            }
        }

        private void btnAddGlobalItemSelected_Click(object sender, ControlEventArgs ev) {
            try {
                int selectedId = Globals.Core.Actions.CurrentSelection;
                WorldObject wo = Globals.Core.WorldFilter[selectedId];
                Globals.Settings.txtAddGlobalItem.Text = wo.Name;
                ListHelper.AddToGlobalItemSearchList(Globals.Settings.txtAddGlobalItem.Text);
            }
            catch (NullReferenceException nre) {
                Util.WriteToChat("Nothing is selected", Util.Color_Pink);
                Util.LogError(nre, false);
            }
            catch (Exception ex) {
                Utilities.Util.LogError(ex);
            }
        }

        private void lstGlobalItems_Selected(object sender, ListSelectEventArgs e) {
            try {
                ListHelper.RemoveFromGlobalItemSearchList(e.Row);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void btnRecheckGlobalSearch_Click(object sender, ControlEventArgs ev) {
            try {
                WorldObjectCollection woc = Globals.Core.WorldFilter.GetAll();
                foreach (WorldObject wo in woc) {
                    ProcessWorldObject(wo);
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void btnGetObjectClass_Click(object sender, ControlEventArgs ev) {
            try {
                Util.WriteToChat(self.GetSelected().ObjectClass.ToString());
            }
            catch (NullReferenceException nre) {
                Util.WriteToChat("Cannot obtain object class", Util.Color_Red);
                Util.LogError(nre, false);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }
    }
}
