﻿using System;
using Ntools.ACObjects.Spells;

namespace Ntools.Engine
{
    public class VitalManagement
    {
        public ACObjects.PlayerSelf Self { get; private set; }
        public int HealthThreshold { get; set; }
        public int StaminaThreshold { get; set; }
        public int ManaThreshold { get; set; }

        public VitalManagement(ACObjects.PlayerSelf self) {
            Self = self;
            HealthThreshold = 75;
            StaminaThreshold = 80;
            ManaThreshold = 50;
            Self.VitalsChanged += Self_VitalsChanged;
        }

        private void Self_VitalsChanged(object sender, EventArgs e) {
            int revit = LifeSpells.RevitalizeSelf.MaxLevelKnown();
            int s2m = LifeSpells.StaminaToMana.MaxLevelKnown();
            int heal = LifeSpells.HealSelf.MaxLevelKnown();

            if (Self.PercentHealth < HealthThreshold) {
                if (!Self.SpellQueue.Contains(heal)) {
                    Self.SpellQueue.Enqueue(heal);
                }
            }


            if (Self.PercentStamina < StaminaThreshold) {
                if (!Self.SpellQueue.Contains(revit)) {
                    Self.SpellQueue.Enqueue(revit);
                }
            }
            
            if (Self.PercentMana < ManaThreshold) {
                if (!Self.SpellQueue.Contains(s2m)) {
                    Self.SpellQueue.Enqueue(s2m);
                }
            }
        }
    }
}
