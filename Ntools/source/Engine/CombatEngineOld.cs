﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using Decal.Adapter;
using Ntools.ACObjects;
using Ntools.ACObjects.Spells;
using Ntools.Utilities;

namespace Ntools.Engine
{
    public class CombatEngineOld
    {
        public int CastSpeed { get; set; }
        public int SpellID;
        public static bool ReadyToCast = true;
        public PlayerSelf Self;
        private VitalManagement vitalManagement;

        public CombatEngineOld(PlayerSelf self) {
            try {
                Util.WriteToChat(String.Format("CombatEngine Engine Online: {0} : 0x{1:X}",
                    self.Name, self.GUID));
                Self = self;
                CastSpeed = 750;
                Globals.Core.EchoFilter.ServerDispatch += EchoFilter_ServerDispatch;
                Globals.Core.CharacterFilter.SpellCast += CharacterFilter_SpellCast;
                vitalManagement = new VitalManagement(self);
            }
            catch (Exception e) {
                Util.LogError(e);
            }
        }

        void CharacterFilter_SpellCast(object sender, Decal.Adapter.Wrappers.SpellCastEventArgs ev) {
            try {
                //SpellID = ev.SpellId;
                //Util.WriteToChat(String.Format("CombatEngine Engine: 0x{0:X}", ev.SpellId));
            }
            catch (Exception e) {
                Util.LogError(e);
            }
        }

        // Working on parsing if ready, checking for possible way to see if ready to cast next spell
        // Events are not a great way to determine this -- abandoning for now
        void EchoFilter_ServerDispatch(object sender, NetworkMessageEventArgs args) {
            try {
                int messageType = args.Message.Type;

                //if (messageType == ACEvents.Messages.Animation) {
                //    int objectID = args.Message.Value<int>("object");
                //    if (objectID == objectID) {
                //        byte animationType = args.Message.Value<byte>("animation_type");

                //        uint flags = args.Message.Value<uint>("flags");
                //        uint stance = args.Message.Value<uint>("stance");
                //        uint stance2 = args.Message.Value<uint>("stance2");

                //        byte[] bytes = args.Message.RawData;
                //        StringBuilder butter = new StringBuilder();
                //        butter.Append(String.Format("animType: {0:X} ID: {1:X} flags: {2:X} stance: {3:X} Stance2: {4:X} Bytes: ",
                //            animationType, objectID, flags, stance, stance2));
                //        foreach (byte b in bytes) {
                //            butter.Append("[");
                //            butter.Append(String.Format("{0:X}", b));
                //            butter.Append("]");
                //        }
                //        Util.LogDebug(butter.ToString());
                //    }

                //}

                //if (messageType == ACEvents.Messages.GameEvent) {
                //    uint eventValue = args.Message.Value<uint>("event");
                //    if (eventValue == ACEvents.StructEnum.GameEvents.DisplayStatusMessage) {
                //        uint statusMessageType1 = args.Message.Value<uint>("type");
                //        Util.LogDebug(String.Format("Status Type: {0:X}", statusMessageType1));
                //        if (statusMessageType1 == ACEvents.StructEnum.DisplayStatusMessage.TooBusy) {
                //            Util.WriteToChat("Too busy");
                //        }
                //    }
                //    if (eventValue == ACEvents.StructEnum.GameEvents.ReadyPreviousActionComplete) {
                //        uint flags = args.Message.Value<uint>("flags");
                //        uint dwrd = args.Message.Value<uint>("sequence");
                //        uint uk = args.Message.Value<uint>("unknown");
                //        byte[] bytes = args.Message.RawData;
                //        StringBuilder butter = new StringBuilder();
                //        Util.WriteToChat("Ready, Previous Action Complete");
                //        butter.Append(String.Format("SpellID: {0:X},  Flags: {1}, DWORD: {2:X}, Unknown: {3} Bytes: ", SpellID, flags, dwrd, uk));
                //        foreach (byte b in bytes) {
                //            butter.Append("[");
                //            butter.Append(String.Format("{0:X}", b));
                //            butter.Append("]");
                //        }
                //        Util.LogDebug(butter.ToString());
                //        Util.WriteToChat(butter.ToString());
                //    }
                //}
            }

            catch (Exception e) {
                Util.LogError(e);
            }
        }
    }
}
