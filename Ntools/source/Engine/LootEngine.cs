﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Ntools.ACObjects.Items;
using Ntools.Utilities;

namespace Ntools.Engine {
    public class LootEngine {
        ACObjects.PlayerSelf Self;
        private PushButtonWrapper btnShowSelectedItem;
        private PushButtonWrapper btnAddSelectedItem;
        private TextBoxWrapper edtShowSelectedItem;

        public Dictionary<int, Item> ItemList;

        public LootEngine(ACObjects.PlayerSelf self) {
            this.Self = self;
            Util.WriteToChat("Loot Engine online: " + this.Self.Name);
            Globals.Core.ContainerOpened += Core_ContainerOpened;
        }

        private void Core_ContainerOpened(object sender, ContainerOpenedEventArgs e) {
            try {
                Util.WriteToChat("DEBUG: Container Opened");
                int containerGUID = e.ItemGuid;
                if (containerGUID != 0) {
                    ItemList = new Dictionary<int, Item>();
                    WorldObject source = Globals.Core.WorldFilter[containerGUID];
                    WorldObjectCollection itemsInContainer =
                        Globals.Core.WorldFilter.GetByContainer(containerGUID);
                    foreach (WorldObject wo in itemsInContainer) {
                        Item item = new Item(wo, source);
                        Util.LogDebug(item.ToString());
                        if (!ItemList.ContainsKey(wo.Id)) {
                            switch (wo.ObjectClass) {
                                case ObjectClass.Armor:
                                    ItemList.Add(item.GUID, new ArmorItem(wo, source));
                                    break;
                                case ObjectClass.MeleeWeapon:
                                    ItemList.Add(item.GUID, new MeleeWeaponItem(wo, source));
                                    break;
                                default:
                                    ItemList.Add(item.GUID, item);
                                    break;
                            }
                        }
                    }
                    new System.Threading.Thread(() => {
                        while (Globals.Core.IDQueue.ActionCount > 0) {
                            // Do nothing
                        }
                        foreach (Item item in ItemList.Values) {
                            Util.WriteToChat(item.ToString());
                        }
                    }).Start();
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }

        }

        public void LootItem(int guid) {
            try {
                Globals.Core.Actions.UseItem(guid, 0);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public void setBtnShowSelectedItem(PushButtonWrapper pushButton) {
            this.btnShowSelectedItem = pushButton;
            btnShowSelectedItem.Click += btnShowSelectedItem_Click;
        }

        public void btnShowSelectedItem_Click(object sender, ControlEventArgs e) {
            string selectedName = Globals.Core.WorldFilter[Globals.Core.Actions.CurrentSelection].Name;
            this.edtShowSelectedItem.Text = selectedName;
        }

        public void setEdtShowSelectedItem(TextBoxWrapper textBox) {
            this.edtShowSelectedItem = textBox;
        }

        public void setBtnAddSelectedItem(PushButtonWrapper pushButton) {
            this.btnAddSelectedItem = pushButton;
        }

    }
}
