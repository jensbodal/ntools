﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Ntools.ACObjects;
using Ntools.Utilities;
using System.Runtime.InteropServices;

namespace Ntools.Engine {
    public class ChatEngine : IEngine {
        
        private PlayerSelf self { get; set; }
        private PluginCore core { get; set; }

        public ChatEngine(PlayerSelf self, PluginCore core) {
            this.self = self;
            this.core = core;
            RegisterEvents();
        }

        public void RegisterEvents() {
            Globals.Core.CharacterFilter.Logoff += CharacterFilter_Logoff;
            Globals.Core.CommandLineText += Core_CommandLineText;
        }

        public void CharacterFilter_Logoff(object sender, LogoffEventArgs e) {
            UnregisterEvents();
        }

        public void UnregisterEvents() {
            Globals.Core.CommandLineText += Core_CommandLineText;
        }

        private void Core_CommandLineText(object sender, ChatParserInterceptEventArgs ev) {
            try {
                string nrl = "/nrl";
                if (ev.Text.Equals(nrl)) {
                    ev.Eat = true;
                    ReloadSchema();
                }

                string writePopulation = "/pop";
                if (ev.Text.Equals(writePopulation)) {
                    ev.Eat = true;
                    int population = Globals.Core.CharacterFilter.ServerPopulation;
                    string toWrite = string.Format("[Server: {0}] [Population: {1}]", self.ServerName, population);
                    Util.WriteToChat(toWrite);
                }

                string testcolor = "/testcolor";
                if (ev.Text.StartsWith(testcolor)) {
                    ev.Eat = true;
                    int subStart = testcolor.Length + 1;
                    string parseText = ev.Text.Substring(subStart);
                    int colorInt = int.Parse(parseText);
                    Util.WriteToChat("Testing Color: " + colorInt, colorInt);
                }

                string showColors = "/showcolors";
                if (ev.Text.StartsWith(showColors)) {
                    for (int i = 0; i < 50; i++) {
                        Util.WriteToChat("Testing Color: " + i, i);
                    }
                }

                string faceObject = "/face";
                if (ev.Text.Equals(faceObject)) {
                    ev.Eat = true;
                    Globals.NavigationEngine.FaceObject(self.GetSelected());
                }

                string gotoObject = "/goto";
                if (ev.Text.Equals(gotoObject)) {
                    ev.Eat = true;
                    Globals.NavigationEngine.GotoObject(self.GetSelected());
                }

                string isFacing = "/isface";
                if (ev.Text.Equals(isFacing)) {
                    ev.Eat = true;
                    Util.WriteToChat(Globals.NavigationEngine.IsFacing(self.GetSelected()));
                }

                string getDistance = "/distance";
                if (ev.Text.Equals(getDistance)) {
                    ev.Eat = true;
                    Util.WriteToChat(Globals.NavigationEngine.GetDistance(self.GetSelected()));
                }

                string calculateSpeed = "/speed";
                if (ev.Text.Equals(calculateSpeed)) {
                    ev.Eat = true;
                    Globals.NavigationEngine.CalculateRunSpeed();
                }

                string testNav = "/testnav";
                if (ev.Text.Equals(testNav)) {
                    ev.Eat = true;
                    Globals.NavigationEngine.StartNavigation();
                }

                string pause = "/pause";
                if (ev.Text.Equals(pause)) {
                    ev.Eat = true;
                    Globals.NavigationEngine.NavigationStopped = true;
                }
            }

            catch (SEHException sehe) {

            }

            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void ReloadSchema() {
            try {
                string schemaLocation =
                    "C:\\Users\\jensb\\Documents\\Visual Studio 2013\\Projects\\Ntools\\Ntools\\mainView.xml";
                core.DefaultView.LoadSchema(schemaLocation);
            }
            catch (SEHException sehe) {
                string schemaLocation = 
                    "C:\\Users\\jensbodal\\Documents\\Visual Studio 2013\\Projects\\ntools\\Ntools\\mainView.xml";
                core.DefaultView.LoadSchema(schemaLocation);
                Util.LogError(sehe, false);
            }
        }
    }
}
