﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

using Decal.Adapter;
using Decal.Adapter.Wrappers;

using Ntools.ACObjects;


using Ntools.Utilities;

namespace Ntools.Engine {
    public class NavigationEngine {
        // Margin of error for when at destination location
        public int destNum = 1;
        public bool NavigationStopped = true;
        public bool NavigationPaused = true;
        public bool CharacterMoving = false;
        public const double DESTINATION_MOE = .002;
        public const double SLOWDOWN_DISTANCE = .035;
        
        public event EventHandler WaypointReached;
        public event EventHandler RouteFinished;
        private PlayerSelf self { get; set; }
        private const string coordsPattern = "([0-9]+[.]*[0-9]*)([Nn|Ss])([, ]*)([0-9]+[.]*[0-9]*)([Ee|Ww])";
        private Dictionary<CoordsObject, string> dictionaryCoordinates;
        private event EventHandler dictCoordsUpdated;
        private const int SenderColumn = 0;
        private const int CoordsColumn = 1;
        private string RoutesFolder = SettingsEngine.OutputFolder + "\\Routes\\";
        private Queue<Waypoint> RouteQueue;
        private bool NavigateInReverse = false;

        public NavigationEngine(PlayerSelf self) {
            this.self = self;
            dictionaryCoordinates = new Dictionary<CoordsObject, string>();
            RegisterEvents();
            RouteQueue = new Queue<Waypoint>();
            GenerateChoiceOfRoutes();
        }

        public void RegisterEvents() {
            try {
                dictCoordsUpdated += NavigationEngine_dictCoordsUpdated;
                WaypointReached += NavigationEngine_WaypointReached;
                RouteFinished += NavigationEngine_RouteFinished;
                Globals.Core.CharacterFilter.Logoff += CharacterFilter_Logoff;
                Globals.Core.EchoFilter.ServerDispatch += EchoFilter_ServerDispatch;
                Globals.Settings.btnFaceCoords.Click += btnFaceCoords_Click;
                Globals.Settings.btnGetCoords.Click += btnGetCoords_Click;
                Globals.Settings.lstCoords.Selected += lstCoords_Selected;
                Globals.Settings.btnAddLocation.Click += btnAddLocation_Click;
                Globals.Settings.btnRouteStartPause.Click += btnRouteStartPause_Click;
                Globals.Settings.btnRouteStop.Click += btnRouteStop_Click;
                Globals.Settings.btnRouteReset.Click += btnRouteReset_Click;
                Globals.Settings.btnSaveRoute.Click += btnSaveRoute_Click;
                Globals.Settings.btnLoadRoute.Click += btnLoadRoute_Click;
                Globals.Settings.btnGotoCoords.Click += btnGotoCoords_Click;
                Globals.Settings.btnGotoSelected.Click += btnGotoSelected_Click;
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public void CharacterFilter_Logoff(object sender, LogoffEventArgs e) {
            try {
                UnregisterEvents();
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public void UnregisterEvents() {
            dictCoordsUpdated -= NavigationEngine_dictCoordsUpdated;
            WaypointReached -= NavigationEngine_WaypointReached;
            RouteFinished -= NavigationEngine_RouteFinished;
            Globals.Core.CharacterFilter.Logoff -= CharacterFilter_Logoff;
            Globals.Core.EchoFilter.ServerDispatch -= EchoFilter_ServerDispatch;
            Globals.Settings.btnFaceCoords.Click -= btnFaceCoords_Click;
            Globals.Settings.btnGetCoords.Click -= btnGetCoords_Click;
            Globals.Settings.lstCoords.Selected -= lstCoords_Selected;
            Globals.Settings.btnAddLocation.Click -= btnAddLocation_Click;
            Globals.Settings.btnRouteStartPause.Click -= btnRouteStartPause_Click;
            Globals.Settings.btnRouteStop.Click -= btnRouteStop_Click;
            Globals.Settings.btnRouteReset.Click -= btnRouteReset_Click;
            Globals.Settings.btnSaveRoute.Click -= btnSaveRoute_Click;
            Globals.Settings.btnLoadRoute.Click -= btnLoadRoute_Click;
            Globals.Settings.btnGotoCoords.Click -= btnGotoCoords_Click;
            Globals.Settings.btnGotoSelected.Click -= btnGotoSelected_Click;
        }

        private void EchoFilter_ServerDispatch(object sender, NetworkMessageEventArgs ev) {
            try {
                int messageType = ev.Message.Type;
                uint eventValue = ev.Message.Value<uint>("event");
                string messageText = ev.Message.Value<string>("text");
                uint chatMessageType = ev.Message.Value<uint>("type");
                string senderName = ev.Message.Value<string>("senderName");
                if (messageText != null) {
                    CoordsObject co = ParseCoordinates(messageText);
                    if (co != null) {
                        if (!dictionaryCoordinates.ContainsKey(co)) {
                            dictionaryCoordinates.Add(co, senderName);
                            dictCoordsUpdated(this, EventArgs.Empty);
                        }
                        string coords = co.ToString();
                        Util.WriteToChat(senderName + ": " + co.ToString());
                    }
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void NavigationEngine_dictCoordsUpdated(object sender, EventArgs ev) {
            try {
                ListRow newRow = Globals.Settings.lstCoords.Add();
                newRow[SenderColumn][0] = dictionaryCoordinates.Last().Value;
                newRow[CoordsColumn][0] = dictionaryCoordinates.Last().Key.ToString();
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        /// <summary>
        /// Attempts to parse a CoordsObject from the given text.  If no object is found then returns null;
        /// </summary>
        /// <param name="text">The text to parse for Coordinates</param>
        /// <returns>Attempts to parse a CoordsObject from the given text.  If no object is found then returns null;</returns>
        public static CoordsObject ParseCoordinates(string text) {
            Regex regex = new Regex(coordsPattern);
            Match match = regex.Match(text);
            string NorthSouth;
            string EastWest;
            string nsHeading;
            string ewHeading;
            double nsMultiplier;
            double ewMultiplier;
            double x;
            double y;
            if (match.Groups.Count == 6) {
                NorthSouth = match.Groups[1].ToString();
                nsHeading = match.Groups[2].ToString();
                EastWest = match.Groups[4].ToString();
                ewHeading = match.Groups[5].ToString();

                if (nsHeading.ToLower().Equals("n")) {
                    nsMultiplier = 1;
                }
                else {
                    nsMultiplier = -1;
                }

                if (ewHeading.ToLower().Equals("e")) {
                    ewMultiplier = 1;
                }
                else {
                    ewMultiplier = -1;
                }

                y = double.Parse(NorthSouth) * nsMultiplier;
                x = double.Parse(EastWest) * ewMultiplier;

                return new CoordsObject(y, x);
            }

            else {
                return default(CoordsObject);
            }
        }



        public void CalculateRunSpeed() {
            CoordsObject startLocation = self.GetCoordinates();
            DateTime startTime = DateTime.Now;
            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
            timer.Interval = 3000;
            Commands.KeyDown(Commands.VK_UP);
            timer.Start();
            timer.Tick += ((sender, e) => {
                Commands.KeyUp(Commands.VK_UP);
                double elapsedTime = (DateTime.Now - startTime).TotalSeconds;
                double distanceTraveled = GetDistance(startLocation);
                double speed = (distanceTraveled / elapsedTime);
                Util.WriteToChat(String.Format("{0:0.00}", speed));
                string runSkill = Globals.Core.CharacterFilter.EffectiveSkill[CharFilterSkillType.Run].ToString();
                double runSpeed = (Globals.Core.CharacterFilter.EffectiveSkill[CharFilterSkillType.Run] + 106.11) / 8814.3;
                Util.WriteToChat(runSpeed.ToString());
                Util.LogDebug("RUN SPEED: " + speed.ToString() + ";" + runSkill);
                timer.Stop();

            });
        }


        public void TestLoadRouteQueue() {
            AddLocationToList(-15.6325121561686, -100.90151702563);
            AddLocationToList(-15.6750785668691, -100.899287589391);
            AddLocationToList(-15.710782066981, -100.937916707993);
            AddLocationToList(-15.7596005121867, -100.943800767263);
            AddLocationToList(-15.7584845860799, -101.004847685496);
            AddLocationToList(-15.7574552377065, -101.061157822609);
            AddLocationToList(-15.8003709952037, -101.087535019716);
        }

        
        public void StartNavigation() {
            try {
                CreateNewRouteQueueFromList();
                if (NavigateInReverse) {
                    ReverseRouteQueue();
                }
                RouteQueue = new Queue<Waypoint>(StartRouteAtClosestPoint(RouteQueue));
                GotoObject(RouteQueue.Dequeue());
            }
            catch (InvalidOperationException ioe) {
                Util.WriteToChat("Navigation error, please reset");
                Util.LogError(ioe, false);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void ReverseRouteQueue() {
            RouteQueue = new Queue<Waypoint>(RouteQueue.Reverse());
        }

        public void CreateNewRouteQueueFromList() {
            RouteQueue = new Queue<Waypoint>();
            ListWrapper routeList = Globals.Settings.lstRoute;
            for (int i = 0; i < routeList.RowCount; i++) {
                string coordinates = (string)routeList[i][1][0];
                double waitTime = (double)routeList[i][2][0];
                RouteQueue.Enqueue(new Waypoint(coordinates,waitTime));
            }
        }

        public static CoordsObject StringToCoordsObjectNSEW(string coords) {
            double ns;
            double ew;
            string[] nsew = coords.Split(',');
            ns = double.Parse(nsew[0]);
            ew = double.Parse(nsew[1]);
            return new CoordsObject(ns, ew);
        }

        private Queue<Waypoint> StartRouteAtClosestPoint(Queue<Waypoint> mRouteQueue) {
            double distance = double.MaxValue;
            Waypoint[] tempArray = mRouteQueue.ToArray();

            mRouteQueue.Clear();
            int startIndex = 0;

            for (int i = 0; i < tempArray.Length; i++) {
                double compareDistance = GetDistance(tempArray[i].GetCoordsObject());
                if (compareDistance <= distance) {
                    distance = compareDistance;
                    startIndex = i;
                }
            }
            for (int i = startIndex; i < tempArray.Length; i++) {
                mRouteQueue.Enqueue(tempArray[i]);
            }

            return mRouteQueue;
        }

        private void LogCoordinates(double ns, double ew) {
            string coordString = string.Format(
                "CoordsObject dest{0} = new CoordsObject({1},{2});", destNum, ns, ew);
            Util.LogDebug(coordString, false);
            destNum += 1;

        }

        private void NavigationEngine_WaypointReached(object sender, EventArgs e) {
            try {
                if (RouteQueue.Count > 0) {
                    GotoObject(RouteQueue.Dequeue());
                }
                else {
                    RouteFinished(this, EventArgs.Empty);
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void NavigationEngine_RouteFinished(object sender, EventArgs e) {
            try {
                if (!NavigateInReverse) {
                    NavigateInReverse = true;
                }
                else {
                    NavigateInReverse = false;
                }
                StartNavigation();
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public void GotoObject(Waypoint wp) {
            CoordsObject co = StringToCoordsObjectNSEW(wp.coordinates);
            GotoObject(co, wp.waitTime);
        }

        public void GotoObject(WorldObject wo) {
            GotoObject(wo.Coordinates());
        }

        public void GotoObject(CoordsObject destCoords) {
            GotoObject(destCoords, 0);
        }

        public void GotoObject(CoordsObject destCoords, double waitTime) {
            try {
                NavigationStopped = false;
                NavigationPaused = false;
                bool waiting = false;
                bool debugging = false;
                int debugFrequency = 2500;
                int msCheck = 5000;
                int accuracy = 50;
                DateTime startTime = DateTime.Now;
                Double actualTime = 0;
                EventHandler<System.EventArgs> navigateViaRenderFrame = null;
                double previousDistance = GetDistance(destCoords);
                

                // We must declare the delegate before subscribing to it
                navigateViaRenderFrame = (object sender, EventArgs e) => {
                    Double elapsedTime = (DateTime.Now - startTime).TotalMilliseconds;
                    Double elapsedMilliseconds = (elapsedTime - (elapsedTime % accuracy));
                    double distanceRemaining = GetDistance(destCoords);
                    if (self.GetCombatState() != PlayerSelf.Peace) {
                        NavigationPaused = true;
                    }
                    else if (Globals.Core.Actions.ChatState) {
                        NavigationPaused = true;
                    }
                    else {
                        NavigationPaused = false;
                    }
                    if (NavigationStopped) {
                        StopMoving();
                        Globals.Core.RenderFrame -= navigateViaRenderFrame;
                        return;
                    }
                    if (NavigationPaused) {
                        StopMoving();
                    }
                    
                    // Frame renders every 3-5 ms, and we want to only take action only to an
                    // accuracy as provided by accuracy variable.  Somewhere between 50-250
                    if (actualTime != elapsedMilliseconds && !NavigationPaused && !waiting) {
                        actualTime = elapsedMilliseconds;

                        if (debugging) {
                            if (actualTime % debugFrequency == 0) {
                                Util.WriteToChat("IsFacing: " + IsFacing(destCoords));
                                Util.WriteToChat("IsMoving: " + CharacterMoving);
                                Util.WriteToChat("We moved: " + (previousDistance == distanceRemaining));
                                Util.WriteToChat("Paused: " + NavigationPaused);
                                Util.WriteToChat("Stopped: " + NavigationStopped);
                                Util.WriteToChat(Globals.Settings.btnRouteStartPause.Text);
                            }
                        }

                        // Check every 'msCheck' to see if something went wrong and we are
                        // no longer moving
                        if (actualTime % msCheck == 0 && (previousDistance == distanceRemaining)) {
                            Util.WriteToChat(
                                "Error: Character movement has stopped, attempting to fix",
                                Util.Color_Red);
                            RestartNavigation(ref navigateViaRenderFrame);
                        }

                        // If we are facing the direction, then start running, else face direction
                        if (!IsFacing(destCoords)) {
                            if (CharacterMoving) {
                                Commands.KeyUp(Commands.VK_UP);
                            }
                            else {
                                FaceCoords(destCoords);
                            }
                            CharacterMoving = false;
                        }
                        else {
                            if (!CharacterMoving) {
                                Commands.KeyDown(Commands.VK_UP);
                                CharacterMoving = true;
                            }
                        }

                        // Once we are close to the destination we will walk so we arrive exactly
                        if (distanceRemaining <= SLOWDOWN_DISTANCE && IsFacing(destCoords)) {
                            Commands.KeyDown(Commands.VK_SHIFT);
                        }

                        // Once we are "at" our destination within our margin of error we 
                        // unsubscribe from our event and trigger the DestinationReached event
                        if (distanceRemaining <= DESTINATION_MOE) {
                            StopMoving();
                            waiting = true;
                        }
                        if (actualTime % msCheck == 0) {
                            previousDistance = distanceRemaining;
                        }
                    } // End main navigation statement
                    if (waiting) {
                        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                        waitTime *= 1000;
                        if (waitTime == 0) {
                            waitTime = 1;
                        }
                        timer.Interval = (int)(waitTime);
                        timer.Tick += ((nothing1, nothing2) => {
                            timer.Stop();
                            Globals.Core.RenderFrame -= navigateViaRenderFrame;
                            WaypointReached(this, EventArgs.Empty);
                            navigateViaRenderFrame = null;
                        });
                        timer.Start();
                        waiting = false;
                    }

                };

                // We must subscribe to the event after declaring its delegate
                Globals.Core.RenderFrame += navigateViaRenderFrame;
            }
            catch (NullReferenceException nullEx) {
                Util.WriteToChat("Nothing is selected");
                Util.LogError(nullEx, false);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void RestartNavigation(ref EventHandler<EventArgs> navigateViaRenderFrame) {
            Globals.Settings.btnRouteStartPause.Text = "Start";
            NavigationStopped = true;
            StopMoving();
            NavigateInReverse = false;
            Globals.Core.RenderFrame -= navigateViaRenderFrame;
            StartNavigation();
        }

        private void StopMoving() {
            Commands.KeyUp(Commands.VK_UP);
            Commands.KeyUp(Commands.VK_SHIFT);
            CharacterMoving = false;
        }

        public double GetRunSpeed() {
            double runSkill = Globals.Core.CharacterFilter.EffectiveSkill[CharFilterSkillType.Run];
            return (0.0112 * Math.Log(runSkill)) - 0.0249;
        }

        public int GetTimeToDestination(CoordsObject destCoords) {
            return (int)(GetDistance(destCoords) / GetRunSpeed() * 1000 * 0.95);
        }

        public void AutoRun(bool enabled) {
            CharacterMoving = enabled;
            Globals.Core.Actions.SetAutorun(enabled);
        }

        public double GetDistance(WorldObject wo) {
            return Globals.Core.WorldFilter.Distance(wo.Id, self.GUID);
        }

        public double GetDistance(CoordsObject destCoords) {
            return self.GetCoordinates().DistanceToCoords(destCoords);
        }

        public double GetDistanceInYards(WorldObject wo) {
            return GetDistanceInYards(wo.Coordinates());
        }

        public double GetDistanceInYards(CoordsObject co) {
            int distanceMultiplier;
            int outdoorMultiplier = 265;
            int indoorMultiplier = 400;
            if (self.IsIndoords()) {
                distanceMultiplier = indoorMultiplier;
            }
            else {
                distanceMultiplier = outdoorMultiplier;
            }
            double distance = GetDistance(co);
            return Math.Round((distance * distanceMultiplier), 2);
        }

        public void FaceObject(WorldObject wo) {
            FaceCoords(wo.Coordinates());
        }

        public void FaceCoords(CoordsObject destCoords) {
            Globals.Core.Actions.FaceHeading(self.AngleToCoordinates(destCoords), false);
        }

        public bool IsFacing(WorldObject wo) {
            return IsFacing(wo.Coordinates());
        }

        public bool IsFacing(CoordsObject destCoords) {
            double targetHeading = self.AngleToCoordinates(destCoords);
            double ourHeading = self.GetHeading();
            double headingDifference = Math.Round(Math.Abs(targetHeading - ourHeading),4);
            if (headingDifference < 1) {
                return true;
            }
            else {
                return false;
            }
        }

        private void AddLocationToList(Double ns, Double ew) {
            AddLocationToList(ns, ew, GetWaitTimeFromGUI());
        }

        private void AddLocationToList(Double ns, Double ew, double waitTime) {
            ListWrapper locationList = Globals.Settings.lstRoute;
            string toAddToList = String.Format("{0},{1}", ns, ew);
            ListRow row = locationList.Add();
            int numCol = 0;
            int coordsCol = 1;
            int waitCol = 2;
            row[numCol][0] = locationList.RowCount;
            row[coordsCol][0] = toAddToList;
            row[waitCol][0] = waitTime;
        }

        private double GetWaitTimeFromGUI() {
            try {
                string txtWaitTime = Globals.Settings.txtWait.Text;
                string defaultTxtWaitTime = "Wait (ms)";
                if (txtWaitTime.Equals(defaultTxtWaitTime)) {
                    return 0;
                }
                else {
                    double waitTime = Math.Round(double.Parse(Globals.Settings.txtWait.Text), 2);
                    Globals.Settings.txtWait.Text = defaultTxtWaitTime;
                    return Math.Abs(waitTime);
                }
            }
            catch (FormatException fe) {
                Util.LogError(fe, false);
                return 0;
            }
            catch (Exception ex) {
                Util.LogError(ex);
                return 0;
            }
        }

        private string GetRouteName() {
            List<string> invalidNames = new List<string>();
            invalidNames.Add("");
            invalidNames.Add("Route Name");
            string routeName = Globals.Settings.txtRouteName.Text;
            foreach (string str in invalidNames) {
                if (routeName.Equals(str)) {
                    routeName = "";
                }
            }
            Globals.Settings.txtRouteName.Text = "Route Name";
            return routeName;
        }

        private void GenerateChoiceOfRoutes() {
            try {
                Globals.Settings.choiceRoute.Clear();
                Directory.CreateDirectory(RoutesFolder);
                foreach (string file in Directory.GetFiles(RoutesFolder)) {
                    string routeName = file.Replace(RoutesFolder, "").Replace(".xml", "");
                    Globals.Settings.choiceRoute.Add(routeName, file);
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void SaveRoute() {
            try {
                string routeName = GetRouteName();
                if (routeName.Equals("")) {
                    throw new ArgumentOutOfRangeException();
                }
                int coordsCol = 1;
                int waitCol = 2;
                ListWrapper routeList = Globals.Settings.lstRoute;
                List<Waypoint> waypoints = new List<Waypoint>();

                for (int i = 0; i < routeList.RowCount; i++) {
                    string coords = (string)routeList[i][coordsCol][0];
                    double waitTime = (double)routeList[i][waitCol][0];
                    waypoints.Add(new Waypoint(coords, waitTime));
                }
                ListHelper.SaveAndSerializeList(waypoints, RoutesFolder, routeName + ".xml");
                GenerateChoiceOfRoutes();
            }
            catch (ArgumentOutOfRangeException argEx) {
                Util.WriteToChat("You must enter a valid route name", Util.Color_Red);
                Util.LogError(argEx, false);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }

        }

        private void LoadRoute() {
            try {
                Globals.Settings.lstRoute.Clear();
                ChoiceWrapper routes = Globals.Settings.choiceRoute;
                string filePath = (string)routes.Data[routes.Selected];
                List<Waypoint> waypoints = ListHelper.LoadAndDeserializeList<Waypoint>(filePath);
                foreach (Waypoint wp in waypoints) {
                    CoordsObject co = StringToCoordsObjectNSEW(wp.coordinates);
                    AddLocationToList(co.NorthSouth, co.EastWest, wp.waitTime);
                }
            }
            catch (InvalidCastException castEx) {
                Util.WriteToChat("You must select a route first", Util.Color_Red);
                Util.LogError(castEx, false);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        // **************************** //
        // *** View Implementations *** //
        // **************************** //
        private void btnFaceCoords_Click(object sender, ControlEventArgs ev) {
            try {
                CoordsObject destHeading = ParseCoordinates(Globals.Settings.txtFaceCoords.Text);
                if (destHeading != null) {
                    FaceCoords(destHeading);
                }

                else {
                    Util.WriteToChat("Coords must be in correct format (e.g. 00.0N, 00.0E)");
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        } 

        private void btnGetCoords_Click(object sender, ControlEventArgs ev) {
            try {
                int selectedID = Globals.Core.Actions.CurrentSelection;
                WorldObject selectedObject = Globals.Core.WorldFilter[selectedID];
                Globals.Settings.txtFaceCoords.Text = selectedObject.Coordinates().ToString();
            }
            catch (NullReferenceException nre) {
                Util.WriteToChat("Nothing is selected");
                Util.LogError(nre, false);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void lstCoords_Selected(object sender, ListSelectEventArgs ev) {
            try {
                ListRow row = Globals.Settings.lstCoords[ev.Row];
                CoordsObject selectedCoords = ParseCoordinates(row[CoordsColumn][0].ToString());
                Globals.Core.Actions.FaceHeading(self.AngleToCoordinates(selectedCoords), false);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void btnAddLocation_Click(object sender, ControlEventArgs e) {
            try {
                CoordsObject myCoordinates = self.GetCoordinates();
                double ns = myCoordinates.NorthSouth;
                double ew = myCoordinates.EastWest;
                Util.WriteToChat(ns + "," + ew);
                LogCoordinates(ns, ew);
                AddLocationToList(ns, ew);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void btnRouteStartPause_Click(object sender, ControlEventArgs e) {
            try {
                NavigateInReverse = false;
                StartNavigation();
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void btnRouteStop_Click(object sender, ControlEventArgs e) {
            try {
                Globals.Settings.btnRouteStartPause.Text = "Start";
                NavigationStopped = true;
                StopMoving();
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void btnRouteReset_Click(object sender, ControlEventArgs e) {
            try {
                Globals.Settings.btnRouteStartPause.Text = "Start";
                NavigationStopped = true;
                StopMoving();
                CreateNewRouteQueueFromList();
                Globals.Settings.lstRoute.Clear();
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void btnLoadRoute_Click(object sender, ControlEventArgs e) {
            try {
                LoadRoute();
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void btnSaveRoute_Click(object sender, ControlEventArgs e) {
            try {
                SaveRoute();
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void btnGotoSelected_Click(object sender, ControlEventArgs e) {
            try {
                WorldObject wo = self.GetSelected();
                //GotoObject(wo);
                Util.WriteToChat("Not yet implemented", Util.Color_Pink);
            }
            catch (Exception ex) {

            }
        }

        private void btnGotoCoords_Click(object sender, ControlEventArgs e) {
            try {
                //GotoObject(ParseCoordinates(Globals.Settings.txtFaceCoords.Text));
                Util.WriteToChat("Not yet implemented", Util.Color_Pink);
            }
            catch (Exception ex) {

            }
        }

        public class Waypoint {
            public string coordinates { get; set; }
            public double waitTime { get; set; }

            private Waypoint() {
                // Left empty for serialization
            }

            public Waypoint (string coordinates, double waitTime) {
                this.coordinates = coordinates;
                this.waitTime = waitTime;
            }

            public CoordsObject GetCoordsObject() {
                return NavigationEngine.StringToCoordsObjectNSEW(coordinates);
            }

            public override string ToString() {
                return coordinates;
            }
        }
    }
}
