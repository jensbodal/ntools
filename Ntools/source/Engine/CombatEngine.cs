﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Decal.Adapter.Wrappers;
using Ntools.ACObjects;
using Ntools.ACObjects.Spells;
using Ntools.Utilities;

namespace Ntools.Engine {
    public class CombatEngine : IEngine {

        public bool IsAttacking;
        public bool HasTarget;
        public bool CombatStarted;

        // Private class variables
        private PlayerSelf self;
        private DateTime startTime;
        private double actualTime;
        private const int accuracy = 50;
        private const int CastFrequency = 3000;
        private double attackDistance = 10;
        private int maxLifeSpellLevel = 1;
        private int maxWarSpellLevel = 3;
        private List<KeyValuePair<WorldObject, Double>> monsterList;
        private Monster currentTarget;

        public CombatEngine(PlayerSelf self) {
            this.self = self;
            RegisterEvents();
            Util.WriteToChat("Combat Engine Online.  Attack distance hard coded to: " + attackDistance);
            HasTarget = false;
        }

        public void RegisterEvents() {
            Globals.Core.CharacterFilter.Logoff += CharacterFilter_Logoff;
            Globals.Settings.chkStartCombat.Change += chkStartCombat_Change;
            Globals.Settings.btnTestDistance.Click += btnTestDistance_Click;
        }

        private void btnTestDistance_Click(object sender, Decal.Adapter.ControlEventArgs e) {
            try {
                double distanceInYards = Globals.NavigationEngine.GetDistanceInYards(self.GetSelected());
                Util.WriteToChat("InDungeon: " + self.IsIndoords() + " Distance: " + distanceInYards);
                if (distanceInYards <= attackDistance) {
                    self.CastSpell(WarSpells.LightningArc.MaxLevelKnown(maxWarSpellLevel), self.GetSelected().Id);
                }
                
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        public void UnregisterEvents() {
            Globals.Core.CharacterFilter.Logoff -= CharacterFilter_Logoff;
            Globals.Settings.chkStartCombat.Change -= chkStartCombat_Change;
        }

        public void CharacterFilter_Logoff(object sender, LogoffEventArgs e) {
            UnregisterEvents();
        }

        private void StartCombat() {
            try {
                Util.WriteToChat("Combat Started", Util.Color_Pink);
                if (!Globals.Settings.chkStartCombat.Checked) { 
                    Globals.Settings.chkStartCombat.Checked = true; 
                }
                startTime = DateTime.Now;
                actualTime = 0;
                Globals.Core.RenderFrame += Core_RenderFrame;
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        // Combat tracking is done through the rendering of the AC frame
        private void Core_RenderFrame(object sender, EventArgs e) {
            try {
                //Monster target = new Monster(self.GetSelected());
                double elapsedMilliseconds = (DateTime.Now - startTime).TotalMilliseconds;
                elapsedMilliseconds = (elapsedMilliseconds - (elapsedMilliseconds % accuracy));

                if (actualTime != elapsedMilliseconds) {
                    actualTime = elapsedMilliseconds;

                    if (!HasTarget) {
                        GetNextTarget();
                    }
                    else {
                        self.ForceCombatMode(PlayerSelf.Magic);
                    }

                    if (!currentTarget.IsAlive) {
                        HasTarget = false;
                    }

                    
                    if (actualTime % CastFrequency == 0) {
                        if (HasTarget) {
                            self.CastSpell(WarSpells.LightningArc.MaxLevelKnown(1), currentTarget.Id);
                        }
                    }
                }
            }

            catch (ArgumentOutOfRangeException noMonstersException) {
                self.ForceCombatMode(PlayerSelf.Peace);
            }

            catch (NullReferenceException nullException) {
                if (currentTarget == null) {
                    self.ForceCombatMode(PlayerSelf.Peace);
                }
            }

            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void StopCombat() {
            try {
                Util.WriteToChat("Combat Stopped", Util.Color_Pink);
                if (Globals.Settings.chkStartCombat.Checked) {
                    Globals.Settings.chkStartCombat.Checked = false;
                }
                Globals.Core.RenderFrame -= Core_RenderFrame;
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private void GetKnownMonsters() {
            try {
                monsterList = new List<KeyValuePair<WorldObject, double>>();
                WorldObjectCollection knownMonsterCollection = 
                    Globals.Core.WorldFilter.GetByObjectClass(ObjectClass.Monster);
                foreach (WorldObject wo in knownMonsterCollection) {
                    double distance = Globals.NavigationEngine.GetDistanceInYards(wo);
                    if (distance <= attackDistance) {
                        monsterList.Add(new KeyValuePair<WorldObject, double>(wo, distance));
                    }
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        private WorldObject GetClosestMonster() {
            monsterList = monsterList.OrderBy(distance => distance.Value).ToList();
            return monsterList[0].Key;
        }

        private void GetNextTarget() {
            GetKnownMonsters();
            if (monsterList.Count == 0) {
                currentTarget = null;
            }
            else {
                currentTarget = new Monster(GetClosestMonster());
            }
            if (currentTarget != null) {
                    HasTarget = true;
            }
        }

        //private void OrderRouteQueueByClosest() {
        //    List<KeyValuePair<CoordsObject, Double>> kvpList = 
        //        new List<KeyValuePair<CoordsObject, double>>();

        //    foreach (CoordsObject co in RouteQueue) {
        //        double ns = co.NorthSouth;
        //        double ew = co.EastWest;
        //        kvpList.Add(new KeyValuePair<CoordsObject, double>(co, GetDistance(co)));
        //    }
        //    RouteQueue.Clear();
        //    kvpList = kvpList.OrderBy(distance => distance.Value).ToList();
        //    foreach (KeyValuePair<CoordsObject, Double> kvp in kvpList) {
        //        double ns = kvp.Key.NorthSouth;
        //        double ew = kvp.Key.EastWest;
        //        CoordsObject newCoords = new CoordsObject(ns, ew);
        //        RouteQueue.Enqueue(newCoords);
        //    }
        //}



        // *** Wire up views *** //
        private void chkStartCombat_Change(object sender, Decal.Adapter.CheckBoxChangeEventArgs e) {
            try {
                if (Globals.Settings.chkStartCombat.Checked) {
                    StartCombat();
                }
                else {
                    StopCombat();
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }
    }
}
