﻿using System;
using Decal.Adapter.Wrappers;

namespace Ntools.Engine
{
    public static class CombatState
    {
        public const Decal.Adapter.Wrappers.CombatState Peace = Decal.Adapter.Wrappers.CombatState.Peace;
        public const Decal.Adapter.Wrappers.CombatState Melee = Decal.Adapter.Wrappers.CombatState.Melee;
        public const Decal.Adapter.Wrappers.CombatState Missile = Decal.Adapter.Wrappers.CombatState.Missile;
        public const Decal.Adapter.Wrappers.CombatState Magic = Decal.Adapter.Wrappers.CombatState.Magic;
    }
}
