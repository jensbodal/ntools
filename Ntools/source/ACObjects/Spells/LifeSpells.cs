﻿using System;

namespace Ntools.ACObjects.Spells
{
    public static class LifeSpells
    {
        public static Spell HealSelf = new Spell(
            "Heal Self", 0x6, 0x485, 0x486, 0x487, 0x488, 0x489, 0x819, 0x10D7);
        public static Spell ImperilOther = new Spell(
            "Imperil Other", 0x19, 0x52B, 0x52C, 0x52D, 0x52E, 0x52F, 0x81A, 0x10D8);
        public static Spell RevitalizeSelf = new Spell(
            "Revitalize Self", 0x499, 0x49A, 0x49B, 0x49C, 0x49D, 0x49E, 0x823, 0x10E1);
        public static Spell StaminaToMana = new Spell(
            "Stamina to Mana", 0x68C, 0x68D, 0x68E, 0x68F, 0x690, 0x691, 0x929, 0x1375);
    }
}
