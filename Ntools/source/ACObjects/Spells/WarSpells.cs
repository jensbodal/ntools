﻿using System;

namespace Ntools.ACObjects.Spells
{
    public static class WarSpells
    {
        // Level 8 unknown: 0x1837 or 0x114A
        public static Spell LightningArc = new Spell(
            "Lightning Arc", 0xAAC, 0xAAD, 0xAAE, 0xAAF, 0xAB0, 0xAB1, 0xAB2, 0x114A);

        // Level 8 unknown: 0x1163 or 0x1836
        public static Spell LightningBolt = new Spell(
            "Lightning Bolt", 0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50, 0x85C, 0x1163); 

        public static Spell SlashBolt = new Spell(
            "Whirling Blade", 0x5C, 0x5D, 0x5E, 0x5F, 0x60, 0x61, 0x862, 0x1169);

        public static Spell SlashArc = new Spell(
            "Whirling Blade Arc", 0xAC1, 0xAC2, 0xAC3, 0xAC4, 0xAC5, 0xAC6, 0xAC7, 0x1146);
    }
}
