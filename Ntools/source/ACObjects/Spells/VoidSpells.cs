﻿using System;

namespace Ntools.ACObjects.Spells
{
    public static class VoidSpells
    {
        public static Spell Corrosion = new Spell(
            "Corrosion", 0x150B, 0x150C, 0x150D, 0x150E, 0x150F, 0x1510, 0x1511, 0x1512);
        public static Spell Corruption = new Spell(
            "Corruption", 0x1513, 0x1514, 0x1515, 0x1516, 0x1517, 0x1518, 0x1519, 0x151A);
        public static Spell DestructiveCurse = new Spell(
            "Destructive Curse", 0x14DB, 0x14DC, 0x14DD, 0x14DE, 0x14DF, 0x14E0, 0x14D9, 0x14DA);
        public static Spell FesteringCurse = new Spell(
            "Festering Curse", 0x14FB, 0x14FC, 0x14FD, 0x14FE, 0x14FF, 0x1500, 0x1501, 0x1502);
        public static Spell WeakeningCurse = new Spell(
            "Weakening Curse", 0x1503, 0x1504, 0x1505, 0x1506, 0x1507, 0x1508, 0x1509, 0x150A);
        public static Spell NetherArc = new Spell(
            "Nether Arc", 0x14F9, 0x14F2, 0x14F3, 0x14F4, 0x14F5, 0x14F6, 0x14F7, 0x14F8);
        public static Spell NetherBolt = new Spell(
            "Nether Bolt", 0x14E5, 0x14E6, 0x14E7, 0x14E8, 0x14E9, 0x14EA, 0x14EB, 0x14EC);
    }
}
