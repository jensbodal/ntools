﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ntools.ACObjects.Spells
{
    public class Spell
    {
        public string Name { get; private set; }
        public int I { get; private set; }
        public int II { get; private set; }
        public int III { get; private set; }
        public int IV { get; private set; }
        public int V { get; private set; }
        public int VI { get; private set; }
        public int VII { get; private set; }
        public int VIII { get; private set; }

        public Spell(string name, int I, int II, int III, int IV, int V, int VI, int VII, int VIII) {
            this.Name = name;
            this.I = I;
            this.II = II;
            this.III = III;
            this.IV = IV;
            this.V = V;
            this.VI = VI;
            this.VII = VII;
            this.VIII = VIII;
        }

        public int Level(int level) {
            switch (level) {
                case 1: return I;
                case 2: return II;
                case 3: return III;
                case 4: return IV;
                case 5: return V;
                case 6: return VI;
                case 7: return VII;
                case 8: return VIII;
                default: throw new ArgumentOutOfRangeException(this.Name + level);
            }
        }

        /// <summary>
        /// Obtains the highest known spell level ID for the given spell subject to a specified limit
        /// for the level of spells to limit casting to.
        /// </summary>
        /// <param name="limit">Max level of spell to start searching from (i.e. if you don't want
        /// case level 8 spells set the limit to 7.</param>
        /// <returns>GUID of highest known spell level for the given spell</returns>
        public int MaxLevelKnown(int limit) {
            for (int i = limit; i > 0; i--) {
                if (Globals.Core.CharacterFilter.IsSpellKnown(Level(i))) {
                    return Level(i);
                }
            }
            throw new ArgumentNullException("Spell level not available: " + this.Name);
        }

        /// <summary>
        /// Obtains the highest known spell level ID for the given spell
        /// </summary>
        /// <returns>GUID of highest known spell level for the given spell</returns>
        public int MaxLevelKnown() {
            return MaxLevelKnown(8);
        }

        public override string ToString() {
            return this.Name;
        }
    }
}
