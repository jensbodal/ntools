﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Decal.Adapter.Wrappers;
namespace Ntools.ACObjects {
    public class Portal {
        public int GUID { get; private set; }
        public string Name { get; private set; }
        public CoordsObject Coords { get; private set; }
        public CoordsObject Destination { get; private set; }
        public string Description { get; private set; }
        public int MinLevelRestrict { get; private set; }

        public Portal(WorldObject wo) {
            this.GUID = wo.Id;
            this.Name = wo.Name;
            this.Coords = wo.Coordinates();
            Globals.Host.Actions.RequestId(this.GUID);
            Globals.Host.Actions.RequestId(0);
            Globals.Core.IDQueue.OnActionRemoved += IDQueue_OnActionRemoved;
        }

        private void IDQueue_OnActionRemoved(object sender, Decal.Adapter.IDQueue.FairRoundRobinScheduleQueue<System.Reflection.Assembly, int>.ActionRemovedEventArgs ev) {
            try {
                if (this.GUID == ev.Action) {
                    WorldObject wo = Globals.Core.WorldFilter[ev.Action];
                    this.Destination = Engine.NavigationEngine.ParseCoordinates(wo.Values(StringValueKey.PortalDestination));
                    this.Description = wo.Values(StringValueKey.SimpleDescription);
                    this.MinLevelRestrict = wo.Values(LongValueKey.MinLevelRestrict);
                }
            }
            catch (Exception ex) {
                Utilities.Util.LogError(ex);
                Utilities.Util.LogError(new Exception(Globals.Core.WorldFilter[ev.Action].Values(StringValueKey.PortalDestination)));
            }
        }

        public override string ToString() {
            string returnString = string.Format("Name: {0} | Coords: {1} | Destination: {2} | MinLevel: {3}",
            Name, Coords.ToString(), Destination, MinLevelRestrict);
            return returnString;
        }
    }
}
