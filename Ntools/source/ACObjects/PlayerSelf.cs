﻿using System;

using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Ntools.Utilities;

namespace Ntools.ACObjects {
    public class PlayerSelf : Player {
        public new int CurrentHealth { get; private set; }
        public new int CurrentStamina { get; private set; }
        public new int CurrentMana { get; private set; }
        public new string Name { get; private set; }
        public string ServerName { get; private set; }
        public int BuffedHealth { get; private set; }
        public int BuffedStamina { get; private set; }
        public int BuffedMana { get; private set; }
        public int PercentHealth { get; private set; }
        public int PercentStamina { get; private set; }
        public int PercentMana { get; private set; }
        public new event EventHandler VitalsChanged;
        public System.Collections.Generic.Queue<int> SpellQueue;

        public const CombatState Peace = CombatState.Peace;
        public const CombatState Melee = CombatState.Melee;
        public const CombatState Missile = CombatState.Missile;
        public const CombatState Magic = CombatState.Magic;
        private static CharacterFilter mCharacterFilter = Globals.Core.CharacterFilter;
        private const CharFilterVitalType Health = CharFilterVitalType.Health;
        private const CharFilterVitalType Stamina = CharFilterVitalType.Stamina;
        private const CharFilterVitalType Mana = CharFilterVitalType.Mana;

        public PlayerSelf (int GUID) : base(GUID) {
            this.Name = mCharacterFilter.Name;
            this.ServerName = mCharacterFilter.Server;
            SpellQueue = new System.Collections.Generic.Queue<int>();
            mCharacterFilter.ChangeVital += CharacterFilter_ChangeVital;
        }

        public void CastSpell(int spellId, int targetId) {
            Globals.Core.Actions.CastSpell(spellId, targetId);
        }

        public CombatState GetCombatState() {
            return Globals.Core.Actions.CombatMode;
        }

        public WorldObject GetSelected() {
            try {
                CoreManager ourCore = CoreManager.Current;
                return ourCore.WorldFilter[ourCore.Actions.CurrentSelection];
            }
            catch (Exception ex) {
                Util.LogError(ex);
                return Globals.Core.WorldFilter[GUID];
            }
        }

        public bool IsIndoords() {
            int checkValue = 0xFF00;
            int landblock = Globals.Core.Actions.Landcell;
            return ((landblock & checkValue) != 0);
        }

        public void ForceCombatMode(CombatState state) {
            if (GetCombatState() != state) {
                Globals.Core.Actions.SetCombatMode(state);
            }
        }

        private void CharacterFilter_ChangeVital(object sender, ChangeVitalEventArgs e) {
            VitalsChanged(this, EventArgs.Empty);
            this.CurrentHealth = mCharacterFilter.Vitals[Health].Current;
            this.CurrentStamina = mCharacterFilter.Vitals[Stamina].Current;
            this.CurrentMana = mCharacterFilter.Vitals[Mana].Current;
            this.BuffedHealth = mCharacterFilter.Vitals[Health].Buffed;
            this.BuffedStamina = mCharacterFilter.Vitals[Stamina].Buffed;
            this.BuffedMana = mCharacterFilter.Vitals[Mana].Buffed;
            this.PercentHealth = GetHealthPercentage();
            this.PercentStamina = GetStaminaPercentage();
            this.PercentMana = GetManaPercentage();
        }

        private int GetHealthPercentage() {
            float percentage = (float)this.CurrentHealth / (float)this.BuffedHealth;
            return (int)(Math.Round(percentage * 100));
        }

        private int GetStaminaPercentage() {
            float percentage = (float)this.CurrentStamina / (float)this.BuffedStamina;
            return (int)(Math.Round(percentage * 100));
        }

        private int GetManaPercentage() {
            float percentage = (float)this.CurrentMana / (float)this.BuffedMana;
            return (int)(Math.Round(percentage * 100));
        }
    }
}
