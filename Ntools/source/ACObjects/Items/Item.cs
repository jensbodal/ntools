﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Decal.Adapter.Wrappers;

namespace Ntools.ACObjects.Items {
    public class Item {
        public int GUID { get; private set; }
        public int Value { get; private set; }
        public int Burden { get; private set; }
        public string Name { get; private set; }
        public string Source { get; private set; }
        public string Class { get; private set; }

        public Item(WorldObject item, WorldObject source) {
            this.Name = item.Name;
            this.GUID = item.Id;
            this.Class = item.ObjectClass.ToString();
            this.Value = item.Values(LongValueKey.Value);
            this.Burden = item.Values(LongValueKey.Burden);
            this.Source = source.Name;
        }

        public void Update() {
            Globals.Host.Actions.RequestId(this.GUID);
            Globals.Host.Actions.RequestId(0);
        }

        public override string ToString() {
            return String.Format(
                "Name: {0} Source: {1} Class: {2} Value: {3} Burden: {4}",
                this.Name, this.Source, this.Class, this.Value, this.Burden);
        }
    }
}
