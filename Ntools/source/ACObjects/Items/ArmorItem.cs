﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Decal.Adapter;
using Decal.Adapter.Wrappers;

namespace Ntools.ACObjects.Items {
    public class ArmorItem : Item {
        public long AL { get; private set; }
        public int Slot { get; private set; }

        public ArmorItem(WorldObject item, WorldObject source) : base(item, source) {
            Globals.Core.EchoFilter.ServerDispatch += EchoFilter_ServerDispatch;
            this.Update();
            this.Slot = item.Values(LongValueKey.Slot);
        }

        private void EchoFilter_ServerDispatch(object sender, NetworkMessageEventArgs ev) {
            try {
                if (ev.Message.Type == ACEvents.MessageTypes.GameEvent) {
                    uint EventValue = ev.Message.Value<uint>("event");
                    int id = ev.Message.Value<int>("object");

                    if (EventValue == ACEvents.StructEnum.GameEvents.IdObject && id == this.GUID) {
                        this.AL = Globals.Core.WorldFilter[this.GUID].Values(LongValueKey.ArmorLevel);
                        Utilities.Util.WriteToChat("AL Updated: " + this.AL);
                        Globals.Core.EchoFilter.ServerDispatch -= EchoFilter_ServerDispatch;
                    }
                }
            }
            catch (Exception ex) {
                Utilities.Util.LogError(ex);
            }
        }

        public override string ToString() {
            return String.Format(
                "Name: {0} Source: {1} AL: {2} Slot: {3} Value: {4} Burden: {5}",
                this.Name, this.Source, this.AL, this.Slot, this.Value, this.Burden);
        }
    }
}
