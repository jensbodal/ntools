﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Decal.Adapter.Wrappers;

namespace Ntools.ACObjects.Items {
    public class MeleeWeaponItem : Item {
        public int MaxDamage { get; private set; }
        public double Variance { get; private set; }
        
        public MeleeWeaponItem(WorldObject item, WorldObject source) : base(item, source) {
            this.MaxDamage = item.Values(LongValueKey.MaxDamage);
            this.Variance = item.Values(DoubleValueKey.Variance);
            
        }

        void WorldFilter_CreateObject(object sender, CreateObjectEventArgs e) {
            throw new NotImplementedException();
        }

        public override string ToString() {
            return String.Format(
                "Name: {0} Source: {1} Max Damage: {2} Variance: {3} Value: {4} Burden: {5}",
                this.Name, this.Source, this.MaxDamage, this.Variance, this.Value, this.Burden);
        }
    }
}
