﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Ntools.Utilities;

namespace Ntools.ACObjects {
    public class Player {

        public int GUID { get; private set; }
        public string Name { get; private set; }
        public string MonarchName { get; private set; }
        public int MonarchGUID { get; private set; }
        public int MaxHealth { get; private set; }
        public int CurrentHealth { get; private set; }
        public int MaxStamina { get; private set; }
        public int CurrentStamina { get; private set; }
        public int MaxMana { get; private set; }
        public int CurrentMana { get; private set; }

        public event EventHandler VitalsChanged;

        public Player(int guid) {
            this.GUID = guid;
            Globals.Core.EchoFilter.ServerDispatch += EchoFilter_ServerDispatch;
            Globals.Core.WorldFilter.ChangeObject += WorldFilter_ChangeObject;
            Refresh();
        }

        public void Refresh() {
            Commands.RequestBackgroundID(this.GUID);
        }

        public CoordsObject GetCoordinates() {
            WorldObject wo = Globals.Core.WorldFilter[this.GUID];
            return wo.Coordinates();
        }

        public double AngleToCoordinates(CoordsObject obj) {
            WorldObject wo = Globals.Core.WorldFilter[this.GUID];
            return wo.Coordinates().AngleToCoords(obj);
        }

        public WorldObject GetWorldObject() {
            return Globals.Core.WorldFilter[this.GUID];
        }

        public double GetHeading() {
            return GetWorldObject().Values(DoubleValueKey.Heading);
        }

        public override string ToString() {
            string playerSummary = String.Format(
                "Name: {0} CurrentHealth: {1} CurrentMana: {2} MaxMana: {3} Monarch: {4}",
                this.Name, this.CurrentHealth, this.CurrentMana, this.MaxMana, this.MonarchName);
            return playerSummary;
        }

        void WorldFilter_ChangeObject(object sender, ChangeObjectEventArgs args) {
            WorldObject wObject = args.Changed;
            if (wObject.Id == this.GUID) {
                this.Name = wObject.Name;
                this.MonarchName = wObject.Values(StringValueKey.MonarchName);
                this.MonarchGUID = wObject.Values(LongValueKey.Monarch);
            }
        }

        void EchoFilter_ServerDispatch(object sender, NetworkMessageEventArgs args) {
            int MessageType = args.Message.Type;
            if (MessageType == ACEvents.MessageTypes.GameEvent) {
                uint EventValue = args.Message.Value<uint>("event");
                int id = args.Message.Value<int>("object");

                if (EventValue == Ntools.ACEvents.StructEnum.GameEvents.IdObject && id == this.GUID)
                {
                    uint flags = args.Message.Value<uint>("flags");
                    if ((flags & 0x0100) == 0x0100) {
                        CurrentHealth = args.Message.Value<int>("health");
                        MaxHealth = args.Message.Value<int>("healthMax");
                        CurrentStamina = args.Message.Value<int>("stamina");
                        MaxStamina = args.Message.Value<int>("staminaMax");
                        CurrentMana = args.Message.Value<int>("mana");
                        MaxMana = args.Message.Value<int>("manaMax");
                        VitalsChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
    }
}
