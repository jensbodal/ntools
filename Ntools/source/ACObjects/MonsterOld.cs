﻿using System;

using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Ntools.Utilities;

namespace Ntools.ACObjects
{
    public class MonsterOld {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public int MaxHealth { get; private set; }
        public int CurrentHealth { get; private set; }
        public bool IsAlive { get; private set; }
        public WorldObject AsWorldObject { get; private set; }

        public static event EventHandler MonsterUpdated;

        public MonsterOld(WorldObject monster) {
            this.Id = monster.Id;
            this.Name = monster.Name;
            this.IsAlive = true;
            this.AsWorldObject = monster;
            Globals.Core.EchoFilter.ServerDispatch += EchoFilter_ServerDispatch;
            Globals.Core.WorldFilter.ChangeObject += WorldFilter_ChangeObject;
            Globals.Core.WorldFilter.ReleaseObject += WorldFilter_ReleaseObject;
            Refresh();
        }

        public void Refresh() {
            Commands.RequestBackgroundID(this.Id);
        }

        public override string ToString() {
            string monsterSummary = String.Format(
                "Name: {0} CurrentHealth: {1} MaxHealth: {2} IsAlive: {3}",
                this.Name, this.CurrentHealth, this.MaxHealth, this.IsAlive);
            return monsterSummary;
        }

        private void WorldFilter_ChangeObject(object sender, ChangeObjectEventArgs ev) {
            WorldObject wObject = ev.Changed;
            if (wObject.Id == this.Id) {
                if (this.Name != wObject.Name) {
                    this.Name = wObject.Name;
                    MonsterUpdated(this, EventArgs.Empty);
                }
            }
        }

        private void WorldFilter_ReleaseObject(object sender, ReleaseObjectEventArgs e) {
            if (e.Released.Id == this.Id) {
                this.IsAlive = false;
            }
        }

        private void EchoFilter_ServerDispatch(object sender, NetworkMessageEventArgs ev) {
            int MessageType = ev.Message.Type;
            if (MessageType == ACEvents.MessageTypes.GameEvent) {
                uint EventValue = ev.Message.Value<uint>("event");
                int id = ev.Message.Value<int>("object");

                if (EventValue == Ntools.ACEvents.StructEnum.GameEvents.IdObject && id == this.Id)
                {
                    uint flags = ev.Message.Value<uint>("flags");
                    if ((flags & 0x0100) == 0x0100) {
                        CurrentHealth = ev.Message.Value<int>("health");
                        MaxHealth = ev.Message.Value<int>("healthMax");
                    }
                }
            }
        }
    }
}
