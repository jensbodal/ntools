﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Decal.Adapter;
using Decal.Adapter.Wrappers;

using Ntools.ACEvents;
using Ntools.Utilities;

namespace Ntools.ACObjects {
    public abstract class ACObject {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public WorldObject AsWorldObject { get; private set; }
        protected bool Exists { get; private set; }

        protected ACObject(WorldObject wo) {
            this.Id = wo.Id;
            this.Name = wo.Name;
            this.AsWorldObject = wo;
            this.Exists = true;
            RegisterEvents(); 
            Commands.RequestBackgroundID(this.Id);
        }

        protected void RegisterEvents() {
            try {
                Globals.Core.WorldFilter.ChangeObject += WorldFilter_ChangeObject;
                Globals.Core.WorldFilter.ReleaseObject += WorldFilter_ReleaseObject;
                Globals.Core.EchoFilter.ServerDispatch += EchoFilter_ServerDispatch;
            }

            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        protected void UnregisterEvents() {
            try {
                Globals.Core.WorldFilter.ChangeObject -= WorldFilter_ChangeObject;
                Globals.Core.WorldFilter.ReleaseObject -= WorldFilter_ReleaseObject;
                Globals.Core.EchoFilter.ServerDispatch -= EchoFilter_ServerDispatch;
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        protected abstract void Update(Message message, uint eventValue);

        protected virtual void WorldFilter_ChangeObject(object sender, ChangeObjectEventArgs args) {
            try {
                WorldObject wo = args.Changed;
                if (wo.Id == this.Id) {
                    
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        protected virtual void WorldFilter_ReleaseObject(object sender, ReleaseObjectEventArgs e) {
            try {
                if (e.Released.Id == this.Id) {
                    Exists = false;
                    UnregisterEvents();
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        protected virtual void EchoFilter_ServerDispatch(object sender, NetworkMessageEventArgs e) {
            try {
                int MessageType = e.Message.Type;
                if (MessageType == ACEvents.MessageTypes.GameEvent) {
                    uint eventValue = e.Message.Value<uint>("event");
                    int id = e.Message.Value<int>("object");
                    if (id == this.Id) {
                        Update(e.Message, eventValue);
                    }
                }
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        protected void WriteMessageValue(Message message) {
            uint eventValue = message.Value<uint>("event");
            string eventMessage = ACEvents.StructEnum.GameEvents.GetMessage(eventValue);
            Util.WriteToChat(eventMessage);
            Util.LogDebug(eventMessage, false);
        }

        public CoordsObject GetCoordinates() {
            return AsWorldObject.Coordinates();
        }

        public override abstract string ToString();
    }
}
