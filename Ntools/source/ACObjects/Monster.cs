﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Ntools.Utilities;

namespace Ntools.ACObjects {
    public class Monster : ACObject {
        public float HealthAsPercent { get; private set; }
        public bool IsAlive { get { return base.Exists; } }

        public Monster(WorldObject wo) : base(wo) {
            Util.WriteToChat("Current target: " + this.Name);
        }

        protected override void Update(Message message, uint eventValue) {
            if (eventValue == ACEvents.StructEnum.GameEvents.UpdateHealth) {
                HealthAsPercent = message.Value<float>("health");
            }
            WriteMessageValue(message);
            Util.LogDebug(this.ToString(),false);
        }

        protected override void WorldFilter_ChangeObject(object sender, ChangeObjectEventArgs args) { }

        public override string ToString() {
            string returnString = String.Format(
                "Name: {0} | CurrentHealth: {1} | IsAlive: {2}",
                this.Name, this.HealthAsPercent, this.IsAlive);
            return returnString;
        }
    }
}
