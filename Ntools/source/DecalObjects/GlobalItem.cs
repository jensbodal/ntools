﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ntools.DecalObjects {
    public class GlobalItem {
        public string Name { get; set; }
        public string Coordinates { get; set; }
        public int Id { get; set; }

        public GlobalItem() {
            // Serializable
        }

        public GlobalItem(string name, string coords, int id) {
            this.Name = name;
            this.Coordinates = coords;
            this.Id = id;
        }
    }
}
