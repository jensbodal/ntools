﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ntools.Utilities.Obsolete
{
    [Obsolete("Please use ACEvents.Messages", true)]
    static class MessageTypes {
        /// <summary>Sent every time an object you are aware of ceases to exist. Merely running out of range does not generate this message - in that case, the client just automatically stops tracking it after receiving no updates for a while (which I presume is a very short while).</summary>
        public const int DestroyObject = 0x0024;

        /// <summary>For stackable items, this changes the number of items in the stack.</summary>
        public const int AdjustStackSize = 0x0197;

        /// <summary>A Player Kill occurred nearby (also sent for suicides). This could be interesting to monitor for tournements.</summary>
        public const int PlayerKilled = 0x019E;

        /// <summary>Indirect '/e' text.</summary>
        public const int IndirectText = 0x01E0;

        /// <summary>Contains the text associated with an emote action.</summary>
        public const int EmoteText = 0x01E2;

        /// <summary>A message to be displayed in the chat window, spoken by a nearby player, NPC or creature</summary>
        public const int CreatureMessage = 0x02BB;

        /// <summary>A message to be displayed in the chat window, spoken by a nearby player, NPC or creature</summary>
        public const int CreatureMessage_Ranged = 0x02BC;

        /// <summary>Set or update a Character DWORD property value</summary>
        public const int SetCharacterDword = 0x02CD;

        /// <summary>Set or update an Object DWORD property value</summary>
        public const int SetObjectDword = 0x02CE;

        /// <summary>Set or update a Character QWORD property value</summary>
        public const int SetCharacterQword = 0x02CF;

        /// <summary>Set or update a Character Boolean property value</summary>
        public const int SetCharacterBoolean = 0x02D1;

        /// <summary>Set or update an Object Boolean property value</summary>
        public const int SetObjectBoolean = 0x02D2;

        /// <summary>Set or update an Object String property value</summary>
        public const int SetObjectString = 0x02D6;

        /// <summary>Set or update an Object Resource property value</summary>
        public const int SetObjectResource = 0x02D8;

        /// <summary>Set or update a Character Link property value</summary>
        public const int SetCharacterLink = 0x02D9;

        /// <summary>Set or update an Object Link property value</summary>
        public const int SetObjectLink = 0x02DA;

        /// <summary>Set or update a Character Position property value</summary>
        public const int SetCharacterPosition = 0x02DB;

        /// <summary>Set or update a Character Skill value</summary>
        public const int SetCharacterSkillLevel = 0x02DD;

        /// <summary>Set or update a Character Skill state</summary>
        public const int SetCharacterSkillState = 0x02E1;

        /// <summary>Set or update a Character Attribute value</summary>
        public const int SetCharacterAttribute = 0x02E3;

        /// <summary>Set or update a Character Vital value</summary>
        public const int SetCharacterVital = 0x02E7;

        /// <summary>Set or update a Character Vital value</summary>
        public const int SetCharacterCurrentVital = 0x02E9;

        /// <summary>Sent when a character rematerializes at the lifestone after death.</summary>
        public const int LifestoneMaterialize = 0xF619;

        /// <summary>Sent whenever a character changes their clothes. It contains the entire description of what their wearing (and possibly their facial features as well). This message is only sent for changes, when the character is first created, the body of this message is included inside the creation message.</summary>
        public const int ChangeModel = 0xF625;

        /// <summary>Uncracked - Character creation screen initilised.</summary>
        public const int CharCreationInitilisation = 0xF643;

        /// <summary>Instructs the client to return to 2D mode - the character list.</summary>
        public const int End3DMode = 0xF653;

        /// <summary>A character was marked for  delete.</summary>
        public const int CharDeletion = 0xF655;

        /// <summary>The character to log in.</summary>
        public const int RequestLogin = 0xF657;

        /// <summary>The list of characters on the current account.</summary>
        public const int CharacterList = 0xF658;

        /// <summary>Failure to log in</summary>
        public const int CharacterLoginFailure = 0xF659;

        /// <summary>Create an object somewhere in the world</summary>
        public const int CreateObject = 0xF745;

        /// <summary></summary>
        public const int LoginCharacter = 0xF746;

        /// <summary>Sent whenever an object is removed from the scene.</summary>
        public const int RemoveItem = 0xF747;

        /// <summary>Set position - the server pathologically sends these after every actions - sometimes more than once. If has options for setting a fixed velocity or an arc for thrown weapons and arrows.</summary>
        public const int SetPositionAndMotion = 0xF748;

        /// <summary>Multipurpose message.  So far object wielding has been decoded.  Lots of unknowns</summary>
        public const int WieldObject = 0xF749;

        /// <summary></summary>
        public const int MoveObjectIntoInventory = 0xF74A;

        /// <summary>Signals your client to end the portal animation for you or another char and also is fired when war spells dissapear as they hit an object blocking their path.</summary>
        public const int ToggleObjectVisibility = 0xF74B;

        /// <summary>These are animations. Whenever a human, wObject or object moves - one of these little messages is sent. Even idle emotes (like head scratching and nodding) are sent in this manner.</summary>
        public const int Animation = 0xF74C;

        /// <summary>An object has jumped</summary>
        public const int Jumping = 0xF74E;

        /// <summary>Applies a sound effect.</summary>
        public const int ApplySoundEffect = 0xF750;

        /// <summary>Instructs the client to show the portal graphic.</summary>
        public const int EnterPortalMode = 0xF751;

        /// <summary>Applies an effect with visual and sound.</summary>
        public const int ApplyVisualOrSoundEffect = 0xF755;

        /// <summary>Game Messages are messages that are sequenced.</summary>
        public const int GameEvent = 0xF7B0;

        /// <summary>Game Actions are outgoing messages that are sequenced.</summary>
        public const int GameAction = 0xF7B1;

        /// <summary>The user has clicked 'Enter'. This message does not contain the ID of the character logging on; that comes later.</summary>
        public const int EnterGame = 0xF7C8;

        /// <summary>Update an existing object's data.</summary>
        public const int UpdateObject = 0xF7DB;

        /// <summary>Send or receive a message using Turbine Chat.</summary>
        public const int TurbineChat = 0xF7DE;

        /// <summary>Switch from the character display to the game display.</summary>
        public const int Start3DMode = 0xF7DF;

        /// <summary>Display a message in the chat window.</summary>
        public const int ServerMessage = 0xF7E0;

        /// <summary>The name of the current world.</summary>
        public const int ServerName = 0xF7E1;

        /// <summary>Add or update a dat file Resource.</summary>
        public const int UpdateResource = 0xF7E2;

        /// <summary>A list of dat files that need to be patched</summary>
        public const int DatFilePatchList = 0xF7E7;
    }
}
