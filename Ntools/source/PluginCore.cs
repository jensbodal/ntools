﻿using System;
using System.Text;
using System.Runtime.InteropServices;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

using Decal;
using Ntools.ACObjects;
using Ntools.DecalObjects;
using Ntools.Engine;
using Ntools.Utilities;

namespace Ntools
{
    [WireUpBaseEvents]

    [FriendlyName("Ntools"), View("Ntools.MainView.xml")]
    public class PluginCore : PluginBase {
        // Constants
        public const string PluginName = "Ntools";
        
        private PlayerSelf Self;
        private SettingsEngine settings;

        /// <summary>
        /// Initializes Decal Plugin, believe this is mandatory.  Also creates new entry in
        /// debug log
        /// </summary>
        protected override void Startup() {
            try {
                Globals.Init(PluginName, Host, Core);
                string log2debug = String.Format("================================================"
                    + "============================{0}" +
                    PluginName + " Online{1}" +
                    "============================================================================",
                    Environment.NewLine, Environment.NewLine);
                Util.LogDebug(log2debug);
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        /// <summary>
        /// These process on plugin shutdown... not sure what needs to go here
        /// </summary>
        protected override void Shutdown() {
            try {
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        /// <summary>
        /// Upon logging in this will create a new reference to our character and instantiate the
        /// various engines and register the plugin views
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        [BaseEvent("Login", "CharacterFilter")]
        private void OnLogin(object sender, LoginEventArgs args) {
            try {
                Self = new ACObjects.PlayerSelf(args.Id);
                InitSettings(Self);

                // Initialize all engines
                StartEngines(Self);

                // Will eventually move this to a new engine but leaving here for now
                Globals.Core.CommandLineText += Core_CommandLineText;

                // Log server information to database in new thread so that it doesn't delay
                // the login process
                string ServerInfo = String.Format("[Server: {0}] [Population: {1}] ",
                    Data.GetServerName(), Data.GetPopulation());
                Util.WriteToChat(ServerInfo);
                Util.WriteToChat("Logging server info to SQL disabled");
                //new Thread(() => {
                //    Util.LogServerInfo(Data.GetServerName(), Data.GetPopulation());
                //}).Start();
            }
            catch (Exception e) {
                Util.LogError(e);
            }
        }

        [BaseEvent("LoginComplete","CharacterFilter")]
        private void OnLoginComplete(object sender, EventArgs ev) {
            try {
                double idleDays = 365;
                double idleTime = idleDays * 24 * 60 * 60;
                Globals.Core.Actions.SetIdleTime(idleTime);
                Util.WriteToChat(String.Format("Idle time set to {0:0} days", idleDays));
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
        }

        /// <summary>
        /// This will initialize our engines on login 
        /// </summary>
        /// <param name="self">The PlayerSelf object should be your character...</param>
        private void StartEngines(PlayerSelf self) {
            Globals.NavigationEngine = new Engine.NavigationEngine(self);
            Globals.DetectionEngine = new Engine.DetectionEngine(self);
            Globals.ChatEngine = new Engine.ChatEngine(self, this);
            Globals.CombatEngine = new Engine.CombatEngine(self);
            
        }

        /// <summary>
        /// Will need to move this to its own set of command line arguments for in-game.  Currently
        /// only use is to call /nrl to reload the MainView.xml schema for testing plugin 
        /// development
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        private void Core_CommandLineText(object sender, ChatParserInterceptEventArgs ev) {

        }

        private string getXYZ(double x, double y, double z) {
            return "no";
        }

        // Leaving here for reference, will move to another engine...
        //
        //void Core_ChatBoxMessage(object sender, ChatTextInterceptEventArgs ev) {
        //    try {
        //        ev.Eat = true;
        //        Util.WriteToChat(String.Format("[{0}] {1}", DateTime.Now.ToString(), ev.Text), ev.Color);
        //    }
        //    catch (Exception ex) {
        //        Util.LogError(ex);
        //    }
        //}



        // ************ MainView.XML References ************ //
        // ************************************************* //
        // Main Page
        [ControlReference("chkStartCombat")]
        protected CheckBoxWrapper chkStartCombat;
        [ControlReference("chkEnableDetection")]
        protected CheckBoxWrapper chkEnableDetection;
        [ControlReference("btnSaveSettings")]
        protected PushButtonWrapper btnSaveSettings;
        [ControlReference("btnSaveWindowPosition")]
        protected PushButtonWrapper btnSaveWindowPosition;
        [ControlReference("chkShowPluginOnLogin")]
        protected CheckBoxWrapper chkShowPluginOnLogin;

        // *** Combat Page *** //
        [ControlReference("btnTestDistance")]
        protected PushButtonWrapper btnTestDistance;

        // *** Navigation Page *** //
        // Coords Tracker
        [ControlReference("txtFaceCoords")]
        protected TextBoxWrapper txtFaceCoords;
        [ControlReference("btnFaceCoords")]
        protected PushButtonWrapper btnFaceCoords;
        [ControlReference("lstCoords")]
        protected ListWrapper lstCoords;
        [ControlReference("btnGetCoords")]
        protected PushButtonWrapper btnGetCoords;
        [ControlReference("btnGotoCoords")]
        protected PushButtonWrapper btnGotoCoords;
        [ControlReference("btnGotoSelected")]
        protected PushButtonWrapper btnGotoSelected;
        // Route Editor
        [ControlReference("txtRouteName")]
        protected TextBoxWrapper txtRouteName;
        [ControlReference("btnSaveRoute")]
        protected PushButtonWrapper btnSaveRoute;
        [ControlReference("btnRouteStartPause")]
        protected PushButtonWrapper btnRouteStartPause;
        [ControlReference("btnRouteStop")]
        protected PushButtonWrapper btnRouteStop;
        [ControlReference("btnRouteReset")]
        protected PushButtonWrapper btnRouteReset;
        [ControlReference("choiceRoute")]
        protected ChoiceWrapper choiceRoute;
        [ControlReference("btnLoadRoute")]
        protected PushButtonWrapper btnLoadRoute;
        [ControlReference("btnAddLocation")]
        protected PushButtonWrapper btnAddLocation;
        [ControlReference("txtWait")]
        protected TextBoxWrapper txtWait;
        [ControlReference("lstRoute")]
        protected ListWrapper lstRoute;

        // *** Detection Engine *** //
        // Global Search
        [ControlReference("txtSearchGlobalItem")]
        protected TextBoxWrapper txtSearchGlobalItem;
        [ControlReference("btnSearchGlobalItem")]
        protected PushButtonWrapper btnSearchGlobalItem;
        [ControlReference("lstGlobalItemView")]
        protected ListWrapper lstGlobalItemView;
        [ControlReference("btnClearGlobalItem")]
        protected PushButtonWrapper btnClearGlobalItem;
        [ControlReference("chkGlobalSearchFaceOnSelect")]
        protected CheckBoxWrapper chkGlobalSearchFaceOnSelect;
        [ControlReference("chkGlobalSearchIncludeInventory")]
        protected CheckBoxWrapper chkGlobalSearchIncludeInventory;
        // Global Settings
        [ControlReference("chkDetectCorpses")]
        protected CheckBoxWrapper chkDetectCorpses;
        [ControlReference("chkDetectLifestones")]
        protected CheckBoxWrapper chkDetectLifestones;
        [ControlReference("chkDetectPortals")]
        protected CheckBoxWrapper chkDetectPortals;
        [ControlReference("txtAddGlobalItem")]
        protected TextBoxWrapper txtAddGlobalItem;
        [ControlReference("btnAddGlobalItem")]
        protected PushButtonWrapper btnAddGlobalItem;
        [ControlReference("btnAddGlobalItemSelected")]
        protected PushButtonWrapper btnAddGlobalItemSelected;
        [ControlReference("lstGlobalItems")]
        protected ListWrapper lstGlobalItems;
        [ControlReference("btnRecheckGlobalSearch")]
        protected PushButtonWrapper btnRecheckGlobalSearch;
        [ControlReference("btnGetObjectClass")]
        protected PushButtonWrapper btnGetObjectClass;

        // *** Loot *** //
        // Armor
        [ControlReference("txtArmorAL")]
        protected TextBoxWrapper txtArmorAL;
        [ControlReference("btnAddArmorFilter")]
        protected PushButtonWrapper btnAddArmorFilter;
        // Weapons
        // Magical
        // Special
        [ControlReference("chkLootScrolls")]
        protected CheckBoxWrapper chkLootScrolls;

        private void InitSettings(PlayerSelf self) {
            settings = new SettingsEngine(this.DefaultView, self);

            // Main settings
            settings.chkEnableDetection = chkEnableDetection;
            settings.chkStartCombat = chkStartCombat;
            settings.btnSaveSettings = btnSaveSettings;
            settings.btnSaveWindowPosition = btnSaveWindowPosition;
            settings.chkShowPluginOnLogin = chkShowPluginOnLogin;

            // *** Combat Engine *** //
            settings.btnTestDistance = btnTestDistance;

            // *** Navigation Engine *** //
            // Coords Tracker
            settings.btnFaceCoords = btnFaceCoords;
            settings.txtFaceCoords = txtFaceCoords;
            settings.lstCoords = lstCoords;
            settings.btnGetCoords = btnGetCoords;
            settings.btnGotoCoords = btnGotoCoords;
            settings.btnGotoSelected = btnGotoSelected;

            // Route Editor
            settings.txtRouteName = txtRouteName;
            settings.btnSaveRoute = btnSaveRoute;
            settings.btnRouteStartPause = btnRouteStartPause;
            settings.btnRouteStop = btnRouteStop;
            settings.btnRouteReset = btnRouteReset;
            settings.choiceRoute = choiceRoute;
            settings.btnLoadRoute = btnLoadRoute;
            settings.btnAddLocation = btnAddLocation;
            settings.txtWait = txtWait;
            settings.lstRoute = lstRoute;

            // *** Detection Engine *** //
            // Global Search
            settings.txtSearchGlobalItem = txtSearchGlobalItem;
            settings.btnSearchGlobalItem = btnSearchGlobalItem;
            settings.lstGlobalItemView = lstGlobalItemView;
            settings.btnClearGlobalItem = btnClearGlobalItem;
            settings.chkGlobalSearchFaceOnSelect = chkGlobalSearchFaceOnSelect;
            settings.chkGlobalSearchIncludeInventory = chkGlobalSearchIncludeInventory;
            settings.btnRecheckGlobalSearch = btnRecheckGlobalSearch;
            settings.btnGetObjectClass = btnGetObjectClass;
            // Global Settings
            settings.chkDetectCorpses = chkDetectCorpses;
            settings.chkDetectLifestones = chkDetectLifestones;
            settings.chkDetectPortals = chkDetectPortals;
            settings.txtAddGlobalItem = txtAddGlobalItem;
            settings.btnAddGlobalItem = btnAddGlobalItem;
            settings.btnAddGlobalItemSelected = btnAddGlobalItemSelected;
            settings.lstGlobalItems = lstGlobalItems;
            // Loot Settings
            settings.txtArmorAL = txtArmorAL;
            settings.btnAddArmorFilter = btnAddArmorFilter;
            settings.chkLootScrolls = chkLootScrolls;
            // These must be last! (loadsettings, Global.Settings = settings;
            Globals.Settings = settings;
            settings.LoadSettings();
            ListHelper.LoadGlobalItemSearchList();
        }
    }
}
