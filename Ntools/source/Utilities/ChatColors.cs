﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ntools.Utilities
{
    public static class ChatColors
    {
        public const int SystemGreen = 0;
        public const int Green = 1;
        public const int White = 2;
        public const int BrightYellow = 3;
        public const int DullYellow = 4;
        public const int Purple = 5;
        public const int Red = 6;
        public const int DarkBlue = 7;
        public const int BrightPeach = 8;
        public const int Peach = 9;
        public const int BrightYellow2 = 10;
        public const int DullYellow2 = 11;
        public const int Grey = 12;
        public const int Cyan = 13;
        public const int LightBlue = 14;
        public const int Red2 = 15;
        public const int Green2 = 16;
        public const int DarkBlue2 = 17;
        public const int Green3 = 18;
        public const int Green4 = 19;
        public const int Purple2 = 20;
        public const int Red3 = 21;
        public const int LightRed = 22;
    }
}
