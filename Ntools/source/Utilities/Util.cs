﻿using System;
using System.IO;
using MySql.Data.MySqlClient;


namespace Ntools.Utilities
{
    public static class Util
    {
        public const int Color_Green = 1;
        public const int Color_White = 2;
        public const int Color_Yellow = 3;
        public const int Color_DarkYellow = 4;
        public const int Color_Pink = 5;
        public const int Color_Red = 6;
        public const int Color_Blue = 7;
        public const int Color_LightRed = 8;


        public static void LogError(Exception ex, bool WriteExceptionToChatWindow = true) {
            try {
                string fileName = "Errors.txt";
                using (StreamWriter writer = new StreamWriter(SetOutputFile(fileName), true)) {
                    writer.WriteLine("============================================================================");
                    writer.WriteLine(DateTime.Now.ToString());
                    writer.WriteLine(ex.ToString());
                    writer.WriteLine("============================================================================");
                    writer.WriteLine("");
                    writer.Close();
                }
                if (WriteExceptionToChatWindow) {
                    Util.WriteToChat("Exception thrown, please check the error log: " +
                        ex.GetType().ToString(), Color_Red);
                }
            }
            catch (Exception e) {
                Util.WriteToChat(e.ToString(), Color_Red);
            }
        }

        static string SetOutputFile(string fileName) {
            string outputFile = "";
            try {
                string userProfile = Environment.GetEnvironmentVariable("USERPROFILE").ToString();
                string outputFolder = userProfile + "\\" + Globals.PluginName;
                outputFile = outputFolder + "\\" + fileName;
                System.IO.Directory.CreateDirectory(outputFolder);
                
            }
            catch (Exception ex) {
                Util.LogError(ex);
            }
            return outputFile;
        }

        public static void WriteSpell(string spell) {
            try {
                string fileName = "Spells.txt";
                using (StreamWriter writer = new StreamWriter(SetOutputFile(fileName), true)) {
                    writer.WriteLine(spell);
                    writer.Close();
                }
            }
            catch (Exception e) {
                Util.WriteToChat(e.ToString());
            }
        }

        public static void LogDebug(string text, bool includeTimeStamp = true) {
            try {
                string fileName = "Debug.txt";
                using (StreamWriter writer = new StreamWriter(SetOutputFile(fileName), true)) {
                    if (includeTimeStamp) { writer.WriteLine(DateTime.Now.ToString()); }
                    writer.WriteLine(text);
                    if (includeTimeStamp) { writer.WriteLine(""); }
                    writer.Close();
                }
            }
            catch (Exception e) {
                Util.WriteToChat(e.ToString());
            }
        }

        public static void LogServerInfo(string serverName, int serverPopulation) {
            MySqlConnection connection;
            string connectionString;
            DateTime now = DateTime.Now;
            string dateStamp = now.ToString("yyyy-MM-dd");
            string timeStamp = now.ToString("HH:mm:ss");

            connectionString = "server=ntools.iodb.org;port=33306;uid=ac;pwd=ac;database=asheron;";

            try {
                connection = new MySqlConnection();
                connection.ConnectionString = connectionString;
                connection.Open();
                Console.WriteLine(connection.State.ToString());
                string commandText = String.Format("insert into serverpopulation (server, population,date,time) values('{0}', {1}, '{2}','{3}');", 
                    serverName, serverPopulation, dateStamp, timeStamp);
                //commandText = "insert into serverpopulation (server, population) values('test', 1234);";
                MySqlCommand command = new MySqlCommand(commandText);
                command.Connection = connection;
                command.ExecuteNonQuery();
            }
            catch (Exception e) {
                Util.LogError(e, false);
            }
        }

        public static void WriteToChat(string message, int color) {
            try {
                Globals.Host.Actions.AddChatText("<{" + Globals.PluginName + "}>: " + message, color);
            }
            catch (Exception ex) {
                LogError(ex);
            }
        }

        public static void WriteToChat(string message) {
            try {
                Globals.Host.Actions.AddChatText("<{" + Globals.PluginName + "}>: " + message, 5);
            }
            catch (Exception ex) { 
                LogError(ex); 
            }
        }

        public static void WriteHex(int hexvalue) {
            try {
                Globals.Host.Actions.AddChatText("<{" + Globals.PluginName + "}>: " + String.Format("{0:X}", hexvalue), 5);
            }
            catch (Exception ex) {
                LogError(ex);
            }
        }

        public static void WriteToChat(int message) {
            try {
                Globals.Host.Actions.AddChatText("<{" + Globals.PluginName + "}>: " + message.ToString(), 5);
            }
            catch (Exception ex) {
                LogError(ex);
            }
        }

        public static void WriteToChat(uint message) {
            try {
                Globals.Host.Actions.AddChatText("<{" + Globals.PluginName + "}>: " + "0x" + message.ToString("X8"), 5);
            }
            catch (Exception ex) {
                LogError(ex);
            }
        }

        public static void WriteToChat(float message) {
            try {
                Globals.Host.Actions.AddChatText("<{" + Globals.PluginName + "}>: " + message.ToString(), 5);
            }
            catch (Exception ex) {
                LogError(ex);
            }
        }

        public static void WriteToChat(double message) {
            try {
                Globals.Host.Actions.AddChatText("<{" + Globals.PluginName + "}>: " + message.ToString(), 5);
            }
            catch (Exception ex) {
                LogError(ex);
            }
        }

        public static void WriteToChat(bool value) {
            string message;
            if (value) {
                message = "True";
            }
            else {
                message = "False";
            }
            try {
                Globals.Host.Actions.AddChatText("<{" + Globals.PluginName + "}>: " + message, 5);
            }
            catch (Exception ex) {
                LogError(ex);
            }
        }
    }
}
