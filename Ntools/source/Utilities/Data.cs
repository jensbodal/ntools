﻿using System;
using System.Collections.Generic;
using System.Text;

using Decal.Adapter;

namespace Ntools.Utilities
{
    public static class Data
    {
        private static CoreManager core = Globals.Core;
        
        public static int GetPopulation() {
            return core.CharacterFilter.ServerPopulation;
        }

        public static string GetServerName() {
            return core.CharacterFilter.Server;
        }

        public static string GetAccountName() {
            return core.CharacterFilter.AccountName;
        }
    }
}
