﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Threading;
using System.ComponentModel;



using Decal.Adapter;

namespace Ntools.Utilities
{
    class Commands : VirtualKeys
    {
        // Virtual Keys: http://msdn.microsoft.com/en-us/library/windows/desktop/dd375731(v=vs.85).aspx
        private static int FRAME = 0;
        private static Thread thread;
        private const uint WM_KEYDOWN = 0x0100;
        private const uint WM_KEYUP = 0x0101;
        private const uint WM_CHAR = 0x0102;
        private const uint SPECIAL_KEY_SHIFT = 0x01000000;
        private const uint LPARAM_SHIFT = 0xC0000000;
        private static IntPtr WINDOW = Globals.Host.Decal.Hwnd;
        private static HandleRef HANDLER = new HandleRef(null, WINDOW);
        private static Queue<byte> messageQueue;

        public static void Subscribe() {
            //Globals.Core.EchoFilter.ServerDispatch += EchoFilter_ServerDispatch;
            //Globals.Core.EchoFilter.ClientDispatch += EchoFilter_ClientDispatch;
            //Globals.Core.ItemSelected += Core_ItemSelected;
            //Globals.Core.ChatBoxMessage += Core_ChatBoxMessage;
            //Globals.Core.ChatNameClicked += Core_ChatNameClicked;
            //Globals.Core.CommandLineText += Core_CommandLineText;
            //Globals.Core.ContainerOpened += Core_ContainerOpened;
            //Globals.Core.CharacterFilter.SpellCast += CharacterFilter_SpellCast;

        }

        public static void Unsubscribe() {
            Globals.Core.EchoFilter.ServerDispatch -= EchoFilter_ServerDispatch;
            Globals.Core.EchoFilter.ClientDispatch -= EchoFilter_ClientDispatch;
            Globals.Core.ItemSelected -= Core_ItemSelected;
            Globals.Core.ChatBoxMessage -= Core_ChatBoxMessage;
            Globals.Core.ChatNameClicked -= Core_ChatNameClicked;
            Globals.Core.CommandLineText -= Core_CommandLineText;
            Globals.Core.ContainerOpened -= Core_ContainerOpened;
            Globals.Core.CharacterFilter.SpellCast -= CharacterFilter_SpellCast;
        }

        public static void EnterJ() {
            PressReturn();
            KeyDown(VK_SHIFT);
            KeyDown(VK_J);
            Character(VK_J);
            KeyUp(VK_SHIFT);
            KeyUp(VK_J);
            PressReturn();
        }

        public static void PressReturn() {
            KeyDown(VK_RETURN);
            //Character(VK_RETURN); // This works however I don't believe it is needed
            KeyUp(VK_RETURN);
        }

        public static void SendKey(byte virtualKey) {
            KeyDown(virtualKey);
            //Character(virtualKey); // This works however I don't believe it is needed
            KeyUp(virtualKey);
        }

        public static void EnterCombat() {
            Thread combat = new Thread(() => Delay(5000));
        }

        public static void RequestBackgroundHealth(int target) {
            ACObjects.Player player = new ACObjects.Player(target);
            
            Console.WriteLine(player.GUID);
            
        }

        public static void RequestBackgroundID(int target) {
            Globals.Host.Actions.RequestId(target);
            Globals.Host.Actions.RequestId(0);
        }

        public static double RequestBackgroundGetDistance(int id1, int id2) {
            return Globals.Core.WorldFilter.Distance(id1, id2, true);
        }

        public static void Delay(int seconds) {
            Globals.Core.Actions.SetCombatMode(Engine.CombatState.Magic);
            DateTime present = DateTime.Now;
            DateTime future = present.AddSeconds(seconds);
            while (DateTime.Compare(present, future) < 0) {
                Thread.Sleep(100);
            }
            Globals.Core.Actions.SetCombatMode(Engine.CombatState.Magic);
        }

        public static void PressUp() {
            KeyDown(VK_UP);
            KeyUp(VK_UP);
        }

        public static void SlideCast(int delay) {
            Thread.Sleep(171);
            SlideLeftStart();

            Thread.Sleep(140);
            KeyDown(VK_RIGHT);
            
            Thread.Sleep(499);
            KeyUp(VK_RIGHT);
            KeyUp(VK_X);
            KeyUp(VK_Z);

            Thread.Sleep(47);
            KeyDown(VK_C);
            KeyUp(VK_LEFT);

            Thread.Sleep(47);
            KeyDown(VK_RIGHT);
            KeyDown(VK_X);

            Thread.Sleep(187);
            KeyDown(VK_UP);

            Thread.Sleep(375);
            SlideRightStop();

            Thread.Sleep(31);
            KeyUp(VK_UP);

            Thread.Sleep(31);
            KeyDown(VK_UP);
            KeyDown(VK_C);

            Thread.Sleep(109);
            KeyDown(VK_LEFT);

            Thread.Sleep(499);
            KeyDown(VK_LEFT);

            Thread.Sleep(999);
            KeyUp(VK_C);
            KeyUp(VK_LEFT);
        }

        public static void SlideLeftStart() {
            KeyDown(VK_Z);
            KeyDown(VK_LEFT);
            KeyDown(VK_X);
        }

        public static void SlideLeftStop() {
            KeyUp(VK_Z);
            KeyUp(VK_LEFT);
            KeyUp(VK_X);
        }

        public static void SlideRightStart() {
            KeyDown(VK_C);
            KeyDown(VK_RIGHT);
            KeyDown(VK_X);
        }

        public static void SlideRightStop() {
            KeyUp(VK_C);
            KeyUp(VK_RIGHT);
            KeyUp(VK_X);
        }

        static void EchoFilter_ClientDispatch(object sender, NetworkMessageEventArgs args) {
            Util.WriteToChat("ClientDispatch_C: MessageType " + args.Message.Type.ToString());
        }

        static void EchoFilter_ServerDispatch(object sender, NetworkMessageEventArgs args) {
            Util.WriteToChat("ServerDispatch_C: MessageType " + args.Message.Type.ToString());
            Util.WriteToChat("Event: " + args.Message["event"].ToString());
            Util.WriteToChat("Object: " + args.Message["object"].ToString());
            

        }

        static void CharacterFilter_SpellCast(object sender, Decal.Adapter.Wrappers.SpellCastEventArgs e) {
            //Util.WriteToChat("SubscribedEvent: SpellCast" + " SpellID: "+e.SpellId+" TargetID: "+e.TargetId);
            new Thread(() => Commands.SlideCast(700)).Start();
        }

        static void Core_ContainerOpened(object sender, ContainerOpenedEventArgs e) {
            Util.WriteToChat("ContainerOpened");
        }

        static void Core_CommandLineText(object sender, ChatParserInterceptEventArgs e) {
            Util.WriteToChat("CommandLineText");
        }

        static void Core_ChatNameClicked(object sender, ChatClickInterceptEventArgs e) {
            Util.WriteToChat("ChatNameClicked");
        }

        static void Core_ChatBoxMessage(object sender, ChatTextInterceptEventArgs e) {
            Util.WriteToChat("ChatBoxMessage");
        }

        static void Core_ItemSelected(object sender, ItemSelectedEventArgs e) {
            Util.WriteToChat("Item Selected");
        }

        public static void SendMessage(string message) {
            Globals.Host.Actions.InvokeChatParser(message);
        }
        public static void TypeMessage(string message) {
            // This method would simply type characters and not press enter
        }

        public static void KeyDown(byte virtualKey) {
            IntPtr wParam = (IntPtr)virtualKey;
            UIntPtr lParam;
            if (SPECIAL_KEYS.Contains(virtualKey)) {
                lParam = (UIntPtr)(Set_lParam(virtualKey) + SPECIAL_KEY_SHIFT);
            }
            else {
                lParam = (UIntPtr)Set_lParam(virtualKey);
            }
            UserInteraction.PostMessage(HANDLER, WM_KEYDOWN, wParam, lParam);
        }

        public static void Character(byte virtualKey) {
            IntPtr wParam = (IntPtr)virtualKey;
            UIntPtr lParam = (UIntPtr)Set_lParam(virtualKey);
            UserInteraction.PostMessage(HANDLER, WM_CHAR, wParam, lParam);
        }

        public static void KeyUp(byte virtualKey) {
            IntPtr wParam = (IntPtr)virtualKey;
            UIntPtr lParam;
            if (SPECIAL_KEYS.Contains(virtualKey)) {
                lParam = (UIntPtr)(Set_lParam(virtualKey) + SPECIAL_KEY_SHIFT + LPARAM_SHIFT);
            }
            else {
                lParam = (UIntPtr)(Set_lParam(virtualKey) + LPARAM_SHIFT);
            }
            UserInteraction.PostMessage(HANDLER, WM_KEYUP, wParam, lParam);
        }

        public static bool IsKeyDown(byte virtualKey) {
            short returnValue = UserInteraction.GetAsyncKeyState(virtualKey);
            if (returnValue != 0) {
                return true;
            }
            else {
                return false;
            }
        }

        // Calculates the lParam
        // Modified from http://forums.decaldev.com/phpBB2/viewtopic.php?f=1&t=2644
        // 0x0D ~= 0x001C0001
        // 0x13 ~= 0x00450001
        private static uint Set_lParam(byte VK) {
            return 0x1 | ((UserInteraction.MapVirtualKey(VK, 0)) << 16);
        }
    }
}
