﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ntools.Utilities.Testing
{
    public class TestEventHandling
    {
        public int frameCounter = 0;

        public TestEventHandling() {
            Globals.Core.RenderFrame += Core_RenderFrame;
        }

        private void Core_RenderFrame(object sender, EventArgs e) {
            frameCounter++;
            if (frameCounter % 60 == 0) {
                Util.WriteToChat(frameCounter.ToString());
            }
        }

        public void Unsubscribe() {
            Globals.Core.RenderFrame -= Core_RenderFrame;
        }
    }
}
