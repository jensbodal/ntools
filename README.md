# Ntools
###### A proof-of-concept Decal plugin for Asheron's Call

## Recently Added
* Started adding loot options, can now detect unknown scrolls and items in vendor
* Goto coordinates working for navigation, route navigating has been initially tested

## ToDo List
* Add route optimization -- find closest coordinates in route, set that as beginning, put rest of route as following that 
* Make searching in vendor a checkbox and triggered with a button click
* Make unknown scrolls filter by known schools
* Need to fix mess of button listeners between ListHelper and SettingsEngine
* Fix line 30 in portal with destination coords... possibly issue with that method
* ~~Add command line to get Server Population~~ (Server population only available at login)
* ~~Add option to show on login~~
* ~~Add Save/Load Settings~~
* Test Multiple accounts
* ~~Detect Lifestones~~
* ~~Detect Portals~~
* ~~Detect Corpses~~
* Loot detection
* Fix detecting loot in corpses
* Auto vital management
* Loot last corpse
* Popup window for new stuff (possibly HUD?)
* Possibly implement save plugin position to profile, but maybe not...
* ~~Figure out why objects persist throug logins~~
* Consider creating MainViewEngine if more view settings needed... otherwise placing in SettingsEngine
* ~~Current Save/Load settings will not work for lists~~
* Add exact name match for global item search
* Utilize hasID data for armor/weapon stuff
* ~~Fix detecting if in inventory~~
* ~~Rename Test Save to "Recheck World for Items"~~

#### Todo if possible...
* Reference ControlReference Wrappers from outside PluginCore (sort of not needed since SettingsEngine has been created)